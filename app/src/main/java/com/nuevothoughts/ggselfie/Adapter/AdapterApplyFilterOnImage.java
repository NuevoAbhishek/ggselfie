package com.nuevothoughts.ggselfie.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.SelectItemFromFilter;
import com.nuevothoughts.ggselfie.Model.ModelForFilter;
import com.nuevothoughts.ggselfie.R;
import com.nuevothoughts.ggselfie.Utility.CustomFont;

import java.util.List;

/**
 * Created by dell1 on 18-Sep-17.
 */

public class AdapterApplyFilterOnImage extends RecyclerView.Adapter<AdapterApplyFilterOnImage.MyViewHolder> {

    private List<ModelForFilter> image_bitmap_list;
    private SelectItemFromFilter selectItemFromFilter;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView text;
        LinearLayout ll_top;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            ll_top = (LinearLayout) itemView.findViewById(R.id.ll_top);
            text = (TextView) itemView.findViewById(R.id.text);
            text.setTypeface(CustomFont.getAppFont(context));
        }
    }

    public AdapterApplyFilterOnImage(Context context, List<ModelForFilter> image, SelectItemFromFilter selectItemFromFilter) {
        this.image_bitmap_list = image;
        this.selectItemFromFilter = selectItemFromFilter;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_image_filter, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Bitmap bmp = image_bitmap_list.get(position).getFilterBitmap();
        holder.imageView.setImageBitmap(bmp);
        holder.text.setText(image_bitmap_list.get(position).getFilterName().toString());
        holder.ll_top.setBackgroundResource(0);

        if (image_bitmap_list.get(position).getFilterSelected()) {

            holder.ll_top.setBackgroundResource(R.drawable.selected_image);
        }
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectItemFromFilter.selectedFilterClickListener(position);

            }
        });


    }

    @Override
    public int getItemCount() {
        Log.e("Adapter ", "" + image_bitmap_list.size());
        ;
        return image_bitmap_list.size();
    }
}
