package com.nuevothoughts.ggselfie.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.SelectItemFromFilter;
import com.nuevothoughts.ggselfie.Interface.SelectedItemFromFrames;
import com.nuevothoughts.ggselfie.Model.ModelForFrames;
import com.nuevothoughts.ggselfie.R;
import com.nuevothoughts.ggselfie.Utility.CustomFont;

import java.util.List;

/**
 * Created by dell1 on 05-Oct-17.
 */

public class AdapterApplyFramesOnImage extends RecyclerView.Adapter<AdapterApplyFramesOnImage.MyViewHolder> {

    List<ModelForFrames> framesList;
    SelectedItemFromFrames selectedItemFromFrames;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView,imageView_frame; TextView text; RelativeLayout rl_top;;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            imageView_frame = (ImageView) itemView.findViewById(R.id.imageView_frame);
            text = (TextView) itemView.findViewById(R.id.text);
            text.setTypeface(CustomFont.getFrameFont(context));
            rl_top = (RelativeLayout) itemView.findViewById(R.id.rl_top);

        }
    }

    public AdapterApplyFramesOnImage(Context context,List<ModelForFrames> framesList, SelectedItemFromFrames selectedItemFromFrames) {
        this.framesList = framesList;
        this.selectedItemFromFrames = selectedItemFromFrames;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_image_frames, parent, false);

        return new AdapterApplyFramesOnImage.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {

        Bitmap bmp = framesList.get(position).getFilterBitmap();
        holder.imageView.setImageBitmap(bmp);
        holder.text.setText(framesList.get(position).getText());

        Glide.with(context).load(Constant.ImageDownloadBaseUrl+framesList.get(position).getFrameName()).into(holder.imageView_frame);

        holder.rl_top.setBackgroundResource(0);

        if (framesList.get(position).getSelected()) {

            holder.rl_top.setBackgroundResource(R.drawable.selected_image);
        }
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedItemFromFrames.selectedFramesClickListener(position);
            }
        });



    }

    @Override
    public int getItemCount() {
        return framesList.size();
    }
}
