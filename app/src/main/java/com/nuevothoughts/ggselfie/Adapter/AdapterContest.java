package com.nuevothoughts.ggselfie.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.ContestEvent;
import com.nuevothoughts.ggselfie.Interface.SelectItemFromFilter;
import com.nuevothoughts.ggselfie.Model.ModelForContest;
import com.nuevothoughts.ggselfie.Model.ModelForFilter;
import com.nuevothoughts.ggselfie.R;
import com.nuevothoughts.ggselfie.Utility.CustomFont;

import java.util.List;

/**
 * Created by dell1 on 12-Oct-17.
 */

public class AdapterContest extends RecyclerView.Adapter<AdapterContest.MyViewHolder> {

    Context context;
    List<ModelForContest> contestList;
    ContestEvent contestEvent;

    public AdapterContest(Context context, List<ModelForContest> contestList, ContestEvent contestEvent) {

        this.contestList = contestList;
        this.context = context;
        this.contestEvent = contestEvent;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, userImage;
        TextView text, name, time, tv_total_likes,tv_like_text;
        LinearLayout ll_top, ll_total_likes, ll_like_dislike;
        ImageButton btn_like;


        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            userImage = (ImageView) itemView.findViewById(R.id.userImage);
            tv_total_likes = (TextView) itemView.findViewById(R.id.tv_total_likes);
            tv_total_likes.setTypeface(CustomFont.getAppFont(context));
            name = (TextView) itemView.findViewById(R.id.name);
            name.setTypeface(CustomFont.getAppFont(context));
            tv_like_text = (TextView) itemView.findViewById(R.id.tv_like_text);
            tv_like_text.setTypeface(CustomFont.getAppFont(context));

            time = (TextView) itemView.findViewById(R.id.time);
            time.setTypeface(CustomFont.getAppFont(context));

            btn_like = (ImageButton) itemView.findViewById(R.id.btn_like);
            ll_like_dislike = (LinearLayout) itemView.findViewById(R.id.ll_like_dislike);
            ll_total_likes = (LinearLayout) itemView.findViewById(R.id.ll_total_likes);

        }
    }

    @Override
    public AdapterContest.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_contest, parent, false);

        return new AdapterContest.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AdapterContest.MyViewHolder holder, final int position) {


        Log.e("Adapter", " " + contestList.get(position).getImageName());
        contestList.get(position).getImageName();
        Glide.with(context).load(Constant.ImageDownloadBaseUrl + contestList.get(position).getImageName()).into(holder.imageView);

        String name = contestList.get(position).getName();
        String date = contestList.get(position).getSelfieDate();
        String totalLikes = contestList.get(position).getTotalLikes();
        String isUserLiked = contestList.get(position).isUserLiked();


        holder.name.setText(name);
        holder.time.setText(date);
        holder.ll_total_likes.setVisibility(View.GONE);
        if (Integer.parseInt(totalLikes)>0) {
            holder.tv_total_likes.setText(totalLikes);
            holder.ll_total_likes.setVisibility(View.VISIBLE);
        }

        if(contestList.get(position).isCallWithUserId()) {// call with user id
            if (isUserLiked.equals("1")) {// this user is already liked this selfie

                holder.btn_like.setImageResource(R.drawable.likebutton_orange);

            } else {

                holder.btn_like.setImageResource(R.drawable.likebutton_gray);
            }
        }else {// call without user id


        }


        holder.ll_like_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contestEvent.likeDislikeClick(position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return contestList.size();
    }
}
