package com.nuevothoughts.ggselfie.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.ChooseGGImage;
import com.nuevothoughts.ggselfie.Model.GGImage;
import com.nuevothoughts.ggselfie.R;

import java.util.List;

/**
 * Created by dell1 on 04-Oct-17.
 */

public class AdapterGGChoose extends RecyclerView.Adapter<AdapterGGChoose.MyViewHolder> {

    List<GGImage> imageList;
    Context context;
    ChooseGGImage chooseGGImage;


    public AdapterGGChoose(Context context, List<GGImage> imageList, ChooseGGImage chooseGGImage) {
        this.imageList = imageList;
        this.context = context;
        this.chooseGGImage = chooseGGImage;


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView; RelativeLayout rl_imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            rl_imageView = (RelativeLayout) itemView.findViewById(R.id.rl_imageView);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gg_choose, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Activity activity = (Activity)context;
        if(activity.isFinishing() || activity.isDestroyed()) {
            return;
        }
        Glide.with(context).load(Constant.ImageDownloadBaseUrl + imageList.get(position).getImageName()).into(holder.imageView);


        holder.rl_imageView.setBackgroundResource(0);
        Log.e("Position selected "," "+ position +" "+imageList.get(position).isImageSelected());
        if(imageList.get(position).isImageSelected()) {
            holder.rl_imageView.setBackgroundResource(R.drawable.selected_image);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                chooseGGImage.selectedGGClickListener(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }
}
