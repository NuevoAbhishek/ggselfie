package com.nuevothoughts.ggselfie.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nuevothoughts.ggselfie.Fragment.GGFacebookFragment;
import com.nuevothoughts.ggselfie.Fragment.GGInstagramFragment;
import com.nuevothoughts.ggselfie.Fragment.GGTimeLineFragment;
import com.nuevothoughts.ggselfie.Fragment.GGTwitterFragment;

/**
 * Created by dell1 on 20-Sep-17.
 */

public class GGSocialMediaAdapter  extends FragmentPagerAdapter {

    public GGSocialMediaAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                // Top Rated fragment activity
                return new GGFacebookFragment();
            case 1:
                // Games fragment activity
                return new GGTwitterFragment();
            case 2:
                // Movies fragment activity
                return new GGInstagramFragment();
            case 3:
                // Movies fragment activity
                return new GGTimeLineFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
