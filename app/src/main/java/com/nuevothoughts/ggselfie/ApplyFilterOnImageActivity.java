package com.nuevothoughts.ggselfie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.merkmod.achievementtoastlibrary.AchievementToast;
import com.mukesh.image_processing.ImageProcessingConstants;
import com.mukesh.image_processing.ImageProcessor;
import com.nuevothoughts.ggselfie.Adapter.AdapterApplyFilterOnImage;
import com.nuevothoughts.ggselfie.Adapter.AdapterApplyFramesOnImage;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.SelectItemFromFilter;
import com.nuevothoughts.ggselfie.Interface.SelectedItemFromFrames;
import com.nuevothoughts.ggselfie.Model.GGImage;
import com.nuevothoughts.ggselfie.Model.ModelForFilter;
import com.nuevothoughts.ggselfie.Model.ModelForFrames;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.MultiTouchListener;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.ColorOverlaySubfilter;
import com.zomato.photofilters.imageprocessors.subfilters.VignetteSubfilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dell1 on 18-Sep-17.
 */

public class ApplyFilterOnImageActivity extends AppCompatActivity implements SelectItemFromFilter, SelectedItemFromFrames {
    RecyclerView rv_filter, rv_frames;
    String TAG = "ApplyFilter";
    EditText ed_write; TextView tv_title;
    View rootView;
    static Bitmap finalBitmapAfterFilter;
    Bitmap bitmapAfterOnlyFilterApply;
    ImageProcessor imageProcessor;
    RelativeLayout rl_select_filter_frame, rl_imageView;
    ImageView imageView, imageView_frame;
    int finalBitmapHeight, finalBitmapWidth;
    ImageButton btn_back;
    AdapterApplyFilterOnImage adapterFilter;
    AdapterApplyFramesOnImage adapterFrames;
    ImageButton btn_next;
    Toolbar toolbar;
    List<ModelForFrames> framesList = new ArrayList<ModelForFrames>();
    ;
    List<ModelForFilter> filterList = new ArrayList<ModelForFilter>();
    static int filterSelectedPosition, frameSelectedPosition;
    Button btn_filter, btn_frame;
    LinearLayout ll_selected_filter, ll_selected_frame;

    static {
        System.loadLibrary("NativeImageProcessor");
    }

    int pixHeight;
    int pixWidth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applyfilteronimage);

        rootView = getWindow().getDecorView().findViewById(R.id.rl_top);

        rv_filter = (RecyclerView) findViewById(R.id.rv_filter);
        rv_frames = (RecyclerView) findViewById(R.id.rv_frames);
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView_frame = (ImageView) findViewById(R.id.imageView_frame);
        btn_next = (ImageButton) findViewById(R.id.btn_next);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_filter = (Button) findViewById(R.id.btn_filter);
        btn_filter.setTypeface(CustomFont.getAppFont(this));
        btn_frame = (Button) findViewById(R.id.btn_frame);
        btn_frame.setTypeface(CustomFont.getAppFont(this));
        rl_select_filter_frame = (RelativeLayout) findViewById(R.id.rl_select_filter_frame);
        rl_imageView = (RelativeLayout) findViewById(R.id.rl_imageView);
        ll_selected_filter = (LinearLayout) findViewById(R.id.ll_selected_filter);
        ll_selected_frame = (LinearLayout) findViewById(R.id.ll_selected_frame);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ed_write = (EditText) findViewById(R.id.ed_write);
        ed_write.setTypeface(CustomFont.getFrameFont(this));

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTypeface(CustomFont.getAppFont(this));
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        pixHeight = displayMetrics.heightPixels;
        pixWidth = displayMetrics.widthPixels;

        /// Log.e(TAG," h=  "+displayMetrics.heightPixels + " w = " + displayMetrics.widthPixels + " density "+ displayMetrics.density + " dp h w "+ dpHeight + " "+ dpWidth);
        //imageView.setImageResource(R.drawable.image);
        imageView.setImageBitmap(CameraActivity.finalBitmap);
        finalBitmapHeight = CameraActivity.finalBitmap.getHeight();
        finalBitmapWidth = CameraActivity.finalBitmap.getWidth();
        rl_imageView.getLayoutParams().height = finalBitmapHeight;
        rl_imageView.getLayoutParams().width = finalBitmapWidth;
        rl_imageView.requestLayout();
        imageView_frame.getLayoutParams().height = finalBitmapHeight;
        imageView_frame.getLayoutParams().width = finalBitmapWidth;
        imageView_frame.requestLayout();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(displayMetrics.widthPixels, (int) (displayMetrics.heightPixels * 0.35));
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        rl_select_filter_frame.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutFrameImageView = new RelativeLayout.LayoutParams(displayMetrics.widthPixels + 200, displayMetrics.heightPixels - (int) ((displayMetrics.heightPixels * 0.35) + toolbar.getHeight()));
        imageView_frame.setLayoutParams(layoutFrameImageView);

        imageView.setOnTouchListener(new MultiTouchListener());
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(ApplyFilterOnImageActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_filter.setLayoutManager(horizontalLayoutManagaer);
        LinearLayoutManager horizontalr = new LinearLayoutManager(ApplyFilterOnImageActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rv_frames.setLayoutManager(horizontalr);

        adapterFilter = new AdapterApplyFilterOnImage(this,filterList, this);
        rv_filter.setAdapter(adapterFilter);

        setFilterBitmap();
        downloadFrames();

        adapterFrames = new AdapterApplyFramesOnImage(this, framesList, this);
        rv_frames.setAdapter(adapterFrames);


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View rootView = getWindow().getDecorView().findViewById(R.id.rl_top);

                finalBitmapAfterFilter = getScreenShot(rootView);
                Intent intent = new Intent(ApplyFilterOnImageActivity.this, FinishActivity.class);
                startActivity(intent);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_selected_frame.setBackgroundColor(Color.parseColor("#adadad"));
                ll_selected_filter.setBackgroundColor(Color.parseColor("#f4b500"));
                rv_frames.setVisibility(View.GONE);
                rv_filter.setVisibility(View.VISIBLE);
            }
        });
        btn_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_selected_filter.setBackgroundColor(Color.parseColor("#adadad"));
                ll_selected_frame.setBackgroundColor(Color.parseColor("#f4b500"));
                rv_filter.setVisibility(View.GONE);
                rv_frames.setVisibility(View.VISIBLE);
            }
        });

        ed_write.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                rl_select_filter_frame.setVisibility(View.GONE);

                ed_write.setFocusableInTouchMode(true);
                ed_write.setCursorVisible(true);

                ed_write.setBackgroundResource(0);
                return false;
            }
        });

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                Log.d(TAG, "keypadHeight = " + keypadHeight);

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    rl_select_filter_frame.setVisibility(View.GONE);
                    toolbar.setVisibility(View.GONE);

                } else {
                    // keyboard is closed
                    rl_select_filter_frame.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);

                    ed_write.setCursorVisible(false);
                    ed_write.setBackgroundResource(0);
                }
            }
        });

    }

    Bitmap bimap;

    private void setFilterBitmap() {
        imageProcessor = new ImageProcessor();

        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        bitmapAfterOnlyFilterApply = bitmap;
        bimap = getResizedBitmap(bitmap, 200);

        Bitmap img1 = bimap.copy(bimap.getConfig(), true);

        ModelForFilter modelForFilter = new ModelForFilter();
        modelForFilter.setFilterBitmap(img1);
        modelForFilter.setFilterName("Normal");
        modelForFilter.setFilterSelected(true);
        filterSelectedPosition = 0;
        filterList.add(modelForFilter);
        /**
         filterList.add( imageProcessor.doGreyScale(bimap));
         filterList.add( imageProcessor.createShadow(bimap));
         filterList.add( imageProcessor.createSepiaToningEffect(bimap,150, 0.12, 0.7, 0.3));

         filterList.add( imageProcessor.doInvert(bimap));
         filterList.add( imageProcessor.applyFleaEffect(bimap));
         filterList.add( imageProcessor.applySnowEffect(bimap));
         filterList.add( imageProcessor.applyGaussianBlur(bimap));
         filterList.add( imageProcessor.engrave(bimap));
         filterList.add( imageProcessor.applyShadingFilter(bimap, Color.MAGENTA));
         filterList.add( imageProcessor.applyShadingFilter(bimap, Color.BLUE));

         // img1.recycle(); img1 = null;

         Bitmap img2 = bimap.copy(bimap.getConfig(), true);
         // myFilter.addSubFilter(new ColorOverlaySubfilter(100, .2f, .8f, .0f));
         // filterList.add(myFilter.processFilter(img2));

         Bitmap img4 = bimap.copy(bimap.getConfig(), true);
         // myFilter.addSubFilter(new VignetteSubfilter(getApplicationContext(), 100));
         // filterList.add(myFilter.processFilter(img4)); can remove it

         Bitmap img3 = bimap.copy(bimap.getConfig(), true);
         // myFilter.addSubFilter(new ColorOverlaySubfilter(100, .0f, .2f, .9f));
         /// filterList.add(myFilter.processFilter(img3));


         */
        Filter myFilter = new Filter();
        Bitmap img5 = bimap.copy(bimap.getConfig(), true);
        myFilter.addSubFilter(new VignetteSubfilter(getApplicationContext(), 200));
        ModelForFilter modelForFilter5 = new ModelForFilter();
        modelForFilter5.setFilterBitmap(myFilter.processFilter(img5));
        modelForFilter5.setFilterName("Vignette");
        modelForFilter5.setFilterSelected(false);
        filterList.add(modelForFilter5);

        ModelForFilter modelForFilter2 = new ModelForFilter();
        modelForFilter2.setFilterBitmap(imageProcessor.doInvert(bimap));
        modelForFilter2.setFilterName("Invert");
        modelForFilter2.setFilterSelected(false);
        filterList.add(modelForFilter2);


        ModelForFilter modelForFilter3 = new ModelForFilter();
        modelForFilter3.setFilterBitmap(imageProcessor.createSepiaToningEffect(bimap, 150, 0.12, 0.7, 0.3));
        modelForFilter3.setFilterName("Sepia");
        modelForFilter3.setFilterSelected(false);
        filterList.add(modelForFilter3);

        adapterFilter.notifyDataSetChanged();


        new TaskExample().execute();

    }

    public class TaskExample extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            setMoreFilter(bimap);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapterFilter.notifyDataSetChanged();
        }
    }

    private void setMoreFilter(Bitmap bimap) {


        // filterList.add(imageProcessor.createSepiaToningEffect(bimap, 150, 0.8, 0.2, 0));
        // filterList.add(imageProcessor.createSepiaToningEffect(bimap, 150, 0.12, 0.7, 0.3));
        // filterList.add(imageProcessor.createSepiaToningEffect(bimap, 150, 0.12, 0.3, 0.7));
        // filterList.add(imageProcessor.doColorFilter(bimap, 1, 0, 0));
        // filterList.add(imageProcessor.applyShadingFilter(bimap, Color.BLUE));

 /*
        filterList.add(imageProcessor.doGreyScale(bimap));
        filterList.add(imageProcessor.applyGaussianBlur(bimap));
        filterList.add(imageProcessor.engrave(bimap));

        filterList.add(imageProcessor.doColorFilter(bimap, 0, 1, 0));
        filterList.add(imageProcessor.doColorFilter(bimap, 0, 0, 1));

        filterList.add(imageProcessor.doColorFilter(bimap, 0.5, 0.5, 0.5));
        filterList.add(imageProcessor.doColorFilter(bimap, 1.5, 1.5, 1.5));
        */

        // filterList.add(imageProcessor.applyFleaEffect(bimap));


        ModelForFilter modelForFilter4 = new ModelForFilter();
        modelForFilter4.setFilterBitmap(imageProcessor.applySnowEffect(bimap));
        modelForFilter4.setFilterName("Snow");
        modelForFilter4.setFilterSelected(false);
        filterList.add(modelForFilter4);

        ModelForFilter modelForFilter6 = new ModelForFilter();
        modelForFilter6.setFilterBitmap(imageProcessor.doGreyScale(bimap));
        modelForFilter6.setFilterName("Grey");
        modelForFilter6.setFilterSelected(false);
        filterList.add(modelForFilter6);


        ModelForFilter modelForFilter7 = new ModelForFilter();
        modelForFilter7.setFilterBitmap(imageProcessor.applyGaussianBlur(bimap));
        modelForFilter7.setFilterName("Blur");
        modelForFilter7.setFilterSelected(false);
        filterList.add(modelForFilter7);

        ModelForFilter modelForFilter8 = new ModelForFilter();
        modelForFilter8.setFilterBitmap(imageProcessor.emboss(bimap));
        modelForFilter8.setFilterName("Emboss");
        modelForFilter8.setFilterSelected(false);
        filterList.add(modelForFilter8);


        ModelForFilter modelForFilter9 = new ModelForFilter();
        modelForFilter9.setFilterBitmap(imageProcessor.engrave(bimap));
        modelForFilter9.setFilterName("Engrave");
        modelForFilter9.setFilterSelected(false);
        filterList.add(modelForFilter9);

        // filterList.add(imageProcessor.createShadow(bimap));

        ModelForFilter modelForFilter10 = new ModelForFilter();
        modelForFilter10.setFilterBitmap(imageProcessor.applyReflection(bimap));
        modelForFilter10.setFilterName("Reflection");
        modelForFilter10.setFilterSelected(false);
        filterList.add(modelForFilter10);

        ModelForFilter modelForFilter11 = new ModelForFilter();
        modelForFilter11.setFilterBitmap(imageProcessor.flip(bimap, ImageProcessingConstants.FLIP_VERTICAL));
        modelForFilter11.setFilterName("Flip");
        modelForFilter11.setFilterSelected(false);
        filterList.add(modelForFilter11);

        ModelForFilter modelForFilter12 = new ModelForFilter();
        modelForFilter12.setFilterBitmap(imageProcessor.roundCorner(bimap, 50));
        modelForFilter12.setFilterName("Corner");
        modelForFilter12.setFilterSelected(false);
        filterList.add(modelForFilter12);

    }

    /**
     * reduces the size of the image
     *
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void selectedFilterClickListener(int position) {

        ed_write.setVisibility(View.GONE);
        imageView.setImageBitmap(filterList.get(position).getFilterBitmap());
        filterList.get(filterSelectedPosition).setFilterSelected(false);// make previous selected to false
        filterList.get(position).setFilterSelected(true);// make previous selected to false
        filterSelectedPosition = position; // set new selected position

        imageView.getLayoutParams().height = finalBitmapHeight;
        imageView.requestLayout();
        bitmapAfterOnlyFilterApply = filterList.get(position).getFilterBitmap();// set bitmap in a variable which will be used for frame adapter
        adapterFilter.notifyDataSetChanged();

       // Toast.makeText(getApplicationContext(),filterList.get(position).getFilterName(),Toast.LENGTH_SHORT).show();

        AchievementToast.makeAchievement(ApplyFilterOnImageActivity.this, filterList.get(position).getFilterName(), AchievementToast.LENGTH_SHORT).show();

        for (int i = 0; i < framesList.size(); i++) {

            framesList.get(i).setFilterBitmap(bitmapAfterOnlyFilterApply);

        }
        adapterFrames.notifyDataSetChanged();

       

    }

    @Override
    public void selectedFramesClickListener(int position) {

        ed_write.setVisibility(View.GONE);
        Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + framesList.get(position).getFrameName()).into(imageView_frame);

        framesList.get(frameSelectedPosition).setSelected(false);// set previous selected position false
        framesList.get(position).setSelected(true);// set new selected position true
        frameSelectedPosition = position;// update the position vlaue

        adapterFrames.notifyDataSetChanged();
        if (framesList.get(position).getText() != null && framesList.get(position).getText().length() > 0) {// setting text on above big frame

            ed_write.setText(framesList.get(position).getText());
            ed_write.setVisibility(View.VISIBLE);

        }

    }

    private void downloadFrames() {

        //now create the object of the custom class
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "app_text_selfie_frames/getTextSelfieFrames\n", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {
                    JSONArray jsonArray = response.getJSONArray("frames");
                    framesList.clear();
                    for (int i = 0; i <= jsonArray.length(); i++) {// first frame is nothing but only image


                        ModelForFrames frames = new ModelForFrames();

                        frames.setFilterBitmap(bitmapAfterOnlyFilterApply);
                        if (i == 0) {// this is for adding a normal frame so that user can frame if he wants
                            frames.setFrameName("");
                            frames.setSelected(true);
                            frameSelectedPosition = 0;
                        } else {
                            Log.e(TAG, " url " + jsonArray.getJSONObject(i - 1).getString("url"));
                            frames.setFrameName(jsonArray.getJSONObject(i - 1).getString("url"));
                            frames.setText(jsonArray.getJSONObject(i - 1).getString("text"));
                            frames.setSelected(false);
                        }


                        framesList.add(frames);


                    }
                    adapterFrames.notifyDataSetChanged();


                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetGGImage");//add the request to the queue


    }

    /***
     *  take screen shot of gauti image with my image
     * @param view
     * @return
     */
    private Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, 0, toolbar.getHeight(), bitmap.getWidth(), (bitmap.getHeight() - (int) (pixHeight * 0.35) - toolbar.getHeight()));
        screenView.setDrawingCacheEnabled(false);
        return croppedBitmap;
    }


}
