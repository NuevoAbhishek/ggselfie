package com.nuevothoughts.ggselfie;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toolbar;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.nuevothoughts.ggselfie.Fragment.GalleryFragment;
import com.nuevothoughts.ggselfie.Utility.CollageView;
import com.nuevothoughts.ggselfie.Utility.ColorFilter2;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.CustomSeekBar;
import com.nuevothoughts.ggselfie.Utility.MultiTouchListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CameraActivity extends AppCompatActivity {
    private static final String TAG = "CameraActivity";
    int RESULT_LOAD_IMAGE = 2000;
    int mSensor;
    boolean IsImageCorrectionToolSet = false;
    boolean IsFontCameraOpened;
    Boolean IsGalleryUsing = false;
    static Bitmap finalBitmapFromCamera;
    Button btn_gallery, btn_camera;
    ImageView ggImage;
    LinearLayout ll_selected_gallery, ll_selected_camera, ll_selected, ll_takePicture;
    FrameLayout frame_gallery;
    RelativeLayout rl_fragment;
    ImageView myImage;
    ImageButton ib_switch_camera, btn_back;
    RelativeLayout rl_top;
    TextView tv_title;
    String CAMERA_FRONT, CAMERA_BACK;
    private ImageButton takePictureButton;
    ImageButton next;
    private TextureView textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    // used to correct the orientation of captured image
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    Toolbar toolbar;
    protected CameraDevice cameraDevice;
    protected CameraCaptureSession cameraCaptureSessions;
    protected CaptureRequest captureRequest;
    protected CaptureRequest.Builder captureRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;
    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    int pixHeight;
    int pixWidth;
    TextView tv_adjust,tv_bright,tv_contrast,tv_saturation,tv_temp,tv_gamma,tv_hue;
    TextView tv_adjust_gg,tv_bright_gg,tv_contrast_gg,tv_saturation_gg,tv_temp_gg,tv_gamma_gg,tv_hue_gg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        textureView = (TextureView) findViewById(R.id.texture);
        myImage = (ImageView) findViewById(R.id.myImage);
        ggImage = (ImageView) findViewById(R.id.ggImage);
        rl_top = (RelativeLayout) findViewById(R.id.rl_top);
        ib_switch_camera = (ImageButton) findViewById(R.id.ib_switch_camera);
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        takePictureButton = (ImageButton) findViewById(R.id.btn_takepicture);
        next = (ImageButton) findViewById(R.id.btn_next);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_gallery = (Button) findViewById(R.id.btn_gallery);
        btn_gallery.setTypeface(CustomFont.getAppFont(this));
        btn_camera = (Button) findViewById(R.id.btn_camera);
        btn_camera.setTypeface(CustomFont.getAppFont(this));

        ll_selected_gallery = (LinearLayout) findViewById(R.id.ll_selected_gallery);
        ll_selected_camera = (LinearLayout) findViewById(R.id.ll_selected_camera);
        ll_takePicture = (LinearLayout) findViewById(R.id.ll_takePicture);
        ll_selected = (LinearLayout) findViewById(R.id.ll_selected);
        frame_gallery = (FrameLayout) findViewById(R.id.frame_gallery);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTypeface(CustomFont.getAppFont(this));
        rl_fragment = (RelativeLayout) findViewById(R.id.rl_fragment);

        tv_adjust = (TextView) findViewById(R.id.tv_adjust);
        tv_bright = (TextView) findViewById(R.id.tv_bright);
        tv_contrast = (TextView) findViewById(R.id.tv_contrast);
        tv_saturation = (TextView) findViewById(R.id.tv_saturation);
        tv_temp = (TextView) findViewById(R.id.tv_temp);
        tv_gamma = (TextView) findViewById(R.id.tv_gamma);
        tv_hue = (TextView) findViewById(R.id.tv_hue);
        tv_adjust.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_bright.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_contrast.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_saturation.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_temp.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_gamma.setTypeface(CustomFont.getAppFont(getApplicationContext()));
        tv_hue.setTypeface(CustomFont.getAppFont(getApplicationContext()));

        assert takePictureButton != null;
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                takePicture();
            }
        });
        ib_switch_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switchCamera();

            }
        });
        next.setEnabled(false);

        // ggImage.setImageResource(R.drawable.gg);
        ggImage.setImageBitmap(GGChooseActivity.bitmap);
        myImage.setOnTouchListener(new MultiTouchListener());
        ggImage.setOnTouchListener(new MultiTouchListener());

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();// get the device screen size
        pixHeight = displayMetrics.heightPixels;
        pixWidth = displayMetrics.widthPixels;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(pixWidth, (int) (pixHeight * 0.25));/// set the height of bottom ralyout ot 35% of screen height
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        ll_takePicture.setLayoutParams(params);

        setImageCorrection();// calling the function to set the views of image correction tools
        /*
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bitmap bitmap = ((BitmapDrawable) myImage.getDrawable()).getBitmap();
                finalBitmapFromCamera = getResizedBitmap(bitmap, 500);
                Intent intent = new Intent(CameraActivity.this, ImageCorrectionActivity.class);
                startActivity(intent);

            }
        });
        */
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                IsGalleryUsing = false;
                btn_camera.setEnabled(false);

                myImage.setVisibility(View.GONE);
                ll_selected_camera.setBackgroundResource(R.color.goldenYellow);
                ll_selected_gallery.setBackgroundResource(R.color.unSelected);

                if (!IsGalleryUsing && textureView.isAvailable()) {
                    if (CAMERA_FRONT != null) {
                        openCamera(CAMERA_FRONT);
                    } else if (CAMERA_BACK != null) {
                        openCamera(CAMERA_BACK);
                    }
                } else if (!IsGalleryUsing) {
                    textureView.setSurfaceTextureListener(textureListener);
                }
            }
        });

        btn_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_camera.setEnabled(true);

                ll_selected_camera.setBackgroundResource(R.color.unSelected);
                ll_selected_gallery.setBackgroundResource(R.color.goldenYellow);

                Fragment fragment = new GalleryFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                // transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_out_up);
                transaction.replace(R.id.frame_gallery, fragment).commit();


                // Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // startActivityForResult(intent, RESULT_LOAD_IMAGE);


                rl_fragment.setVisibility(View.VISIBLE);
                Animation slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_up);
                rl_fragment.setAnimation(slideDownAnimation);
                rl_fragment.startAnimation(slideDownAnimation);

                closeCamera();
                IsGalleryUsing = true;
                /**


                 Fragment fragment = new GalleryFragment();
                 FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                 transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_out_up);
                 transaction.replace(R.id.frame_gallery, fragment).commit();
                 **/
            }

        });


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (IsImageCorrectionToolSet) {

                    Intent intent = new Intent(getApplication(),CameraActivity.class);
                    finish();
                    startActivity(intent);
                }else {

                    finish();
                }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImg = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImg, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            myImage.setVisibility(View.VISIBLE);
            myImage = (ImageView) findViewById(R.id.myImage);
            myImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            cursor.close();
            sendToImageCorrection();
            myImage.setOnTouchListener(new MultiTouchListener());
            ggImage.setOnTouchListener(null);
        }
    }

    /**
     * this function is calling from gallery fragment
     */
    public void closeGalleryFragment() {

        Animation slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_down);
        rl_fragment.setAnimation(slideDownAnimation);
        rl_fragment.startAnimation(slideDownAnimation);
        rl_fragment.setVisibility(View.GONE);


        setCameraState();


    }

    /*
       this function set the camera preview and make ready to capture camera
     */
    private void setCameraState() {

        rl_fragment.setVisibility(View.GONE);
        ll_selected_camera.setBackgroundResource(R.color.goldenYellow);
        ll_selected_gallery.setBackgroundResource(R.color.unSelected);


        textureView.setVisibility(View.VISIBLE);
        textureView.setSurfaceTextureListener(textureListener);
        IsGalleryUsing = false;
        myImage.setImageResource(0);
        myImage.setVisibility(View.GONE);
        ib_switch_camera.setVisibility(View.VISIBLE);

        reopenCamera(cameraId);


    }

    private void setGalleryState() {

        IsGalleryUsing = true;
        Fragment fragment = new GalleryFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        // transaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_out_up);
        transaction.replace(R.id.frame_gallery, fragment).commit();
        rl_fragment.setVisibility(View.VISIBLE);
    }

    /**
     * this function called from galley fragment to set the image from gallery
     *
     * @param path
     */
    public void setImageFromGallery(String path) {

        myImage.setVisibility(View.VISIBLE);
        myImage = (ImageView) findViewById(R.id.myImage);
        myImage.setImageBitmap(BitmapFactory.decodeFile(path));

        sendToImageCorrection();
        myImage.setOnTouchListener(new MultiTouchListener());
        ggImage.setOnTouchListener(null);


        rl_fragment.setVisibility(View.GONE);

    }

    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            if (CAMERA_FRONT != null) {
                openCamera(CAMERA_FRONT);
            } else if (CAMERA_BACK != null) {
                openCamera(CAMERA_BACK);
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };


    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreview();
            setAspectRatioTextureView(imageDimension.getWidth(), imageDimension.getHeight());

        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };


    final CameraCaptureSession.CaptureCallback captureCallbackListener = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
            // Toast.makeText(CameraActivity.this, "Saved:" + file, Toast.LENGTH_SHORT).show();
            createCameraPreview();
        }
    };

    protected void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    protected void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void takePicture() {
        if (null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null");
            return;
        }
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegSizes = null;
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }
            int width = 640;
            int height = 480;
            if (jpegSizes != null && 0 < jpegSizes.length) {
                width = jpegSizes[0].getWidth();
                height = jpegSizes[0].getHeight();
            }
            ImageReader reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1);
            List<Surface> outputSurfaces = new ArrayList<Surface>(2);
            outputSurfaces.add(reader.getSurface());
            outputSurfaces.add(new Surface(textureView.getSurfaceTexture()));


            final CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(reader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
            // Orientation
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            Log.e("MainCamera ", " " + rotation);
            // captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ((ORIENTATIONS.get(rotation) + mSensor + 270) % 360));
            final File file = new File(Environment.getExternalStorageDirectory() + "/pic.jpg");

            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader reader) {
                    Image image = null;
                    try {
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        final byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        save(bytes);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                                Matrix matrixStopFlip = new Matrix();
                                if (IsFontCameraOpened) { // reverse flipping of image should only need to be handle for front facing camera
                                    matrixStopFlip.preScale(1.0f, -1.0f);
                                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrixStopFlip, true);

                                }

                                bitmap = getProperRotatation(bitmap, file);
                                ///Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

                                myImage.setImageBitmap(bitmap);
                                myImage.setVisibility(View.VISIBLE);
                                sendToImageCorrection();
                                myImage.setOnTouchListener(new MultiTouchListener());
                                ggImage.setOnTouchListener(null);
                            }
                        });

                        //
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (image != null) {
                            image.close();
                        }
                    }
                }

                private void save(byte[] bytes) throws IOException {
                    OutputStream output = null;
                    try {
                        output = new FileOutputStream(file);
                        output.write(bytes);
                    } finally {
                        if (null != output) {
                            output.close();
                        }
                    }
                }
            };
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    // Toast.makeText(CameraActivity.this, "Saved:" + file, Toast.LENGTH_SHORT).show();
                    // createCameraPreview();
                }
            };

            cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    protected void createCameraPreview() {
        try {

            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
            //texture.setDefaultBufferSize(pixWidth, pixHeight/2);
            Surface surface = new Surface(texture);

            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return;
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    //   Toast.makeText(CameraActivity.this, "Configuration change", Toast.LENGTH_SHORT).show();
                }
            }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void checkCameraAvailable() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        try {
            if (manager != null && manager.getCameraIdList().length > 0) {


                for (String cameraId : manager.getCameraIdList()) {
                    CameraCharacteristics characteristics
                            = manager.getCameraCharacteristics(cameraId);

                    Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                    if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                        CAMERA_FRONT = cameraId;


                    }
                    if (facing != null && facing == CameraCharacteristics.LENS_FACING_BACK) {
                        CAMERA_BACK = cameraId;
                    }
                }
            }
            {
                Toast.makeText(getApplicationContext(), "No camera found", Toast.LENGTH_LONG);
            }
        } catch (Exception e) {

            e.fillInStackTrace();
        }
    }

    private void openCamera(String camera_Id) {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        Log.e(TAG, "is camera open");
        try {

            //cameraId = manager.getCameraIdList()[0];
            IsFontCameraOpened = false;
            cameraId = camera_Id;
            if (camera_Id.equals(CAMERA_FRONT)) {
                IsFontCameraOpened = true;
            }


            CameraCharacteristics characteristics = manager.getCameraCharacteristics(camera_Id);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            mSensor = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            /*
            Size value[] = map.getOutputSizes(SurfaceTexture.class);
            for (Size x : value) {
                Log.e("Camera resolution ", " " + x.getWidth() + " " + x.getHeight());
            }
            */
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(CameraActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
                return;
            }

            manager.openCamera(camera_Id, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "openCamera X");

    }

    protected void updatePreview() {
        if (null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return");
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {

            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(CameraActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");


        checkCameraAvailable();
        startBackgroundThread();

        if (!IsGalleryUsing && textureView.isAvailable()) {

            if (CAMERA_FRONT != null) {
                openCamera(CAMERA_FRONT);
            } else if (CAMERA_BACK != null) {
                openCamera(CAMERA_BACK);
            }
        } else if (!IsGalleryUsing) {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }


    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    public void switchCamera() {
        if (cameraId.equals(CAMERA_FRONT)) {
            cameraId = CAMERA_BACK;
            closeCamera();
            reopenCamera(cameraId);
            // switchCameraButton.setImageResource(R.drawable.ic_camera_front);

        } else if (cameraId.equals(CAMERA_BACK)) {
            cameraId = CAMERA_FRONT;
            closeCamera();
            reopenCamera(cameraId);
            // switchCameraButton.setImageResource(R.drawable.ic_camera_back);
        }
    }

    public void reopenCamera(String cameraId) {
        if (textureView.isAvailable()) {
            openCamera(cameraId);
        } else {
            textureView.setSurfaceTextureListener(textureListener);
        }
    }

    private int xDelta;
    private int yDelta;
    private int rightMargin;


    private View.OnTouchListener onTouchListener() {
        return new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                final int x = (int) event.getRawX();
                final int y = (int) event.getRawY();

                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams)
                                view.getLayoutParams();

                        xDelta = x - lParams.leftMargin;
                        yDelta = y - lParams.topMargin;
                        rightMargin = lParams.rightMargin;
                        Log.e("action_down", "x = " + x + " y = " + y + " lParams.leftMargin " + lParams.leftMargin + " lParams.topMargin " + lParams.topMargin);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Toast.makeText(CameraActivity.this, "thanks for new location!", Toast.LENGTH_SHORT).show();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                                .getLayoutParams();
                        layoutParams.leftMargin = x - xDelta;
                        layoutParams.topMargin = y - yDelta;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = 0;
                        view.setLayoutParams(layoutParams);
                        break;
                }
                // rl_top.invalidate();
                return true;
            }
        };
    } // these matrices will be used to move and zoom image

    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    private ImageView view, fin;
    private Bitmap bmap;

    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            view = (ImageView) v;
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2 || event.getPointerCount() == 3) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];
                            float xc = (view.getWidth() / 2) * sx;
                            float yc = (view.getHeight() / 2) * sx;
                            matrix.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }

            view.setImageMatrix(matrix);

            bmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bmap);
            view.draw(canvas);

            myImage.setImageBitmap(bmap);
            return true;
        }


    };

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        float s = x * x + y * y;
        return (float) Math.sqrt(s);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /**
     * Calculate the degree to be rotated by.
     *
     * @param event
     * @return Degrees
     */
    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    /**
     * reduces the size of the image
     *
     * @param image
     * @param maxSize
     * @return
     */
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private Bitmap getProperRotatation(Bitmap bitmap, File file) {
        Bitmap rotatedBitmap = null;
        try {
            ExifInterface ei = new ExifInterface(file.getAbsolutePath().toString());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);


            switch (orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotatedBitmap = rotateImage(bitmap, 90);
                   // Toast.makeText(getApplicationContext(), "orientation 90 " + orientation, Toast.LENGTH_LONG).show();
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotatedBitmap = rotateImage(bitmap, 180);
                   // Toast.makeText(getApplicationContext(), "orientation 180 " + orientation, Toast.LENGTH_LONG).show();
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotatedBitmap = rotateImage(bitmap, 270);
                   // Toast.makeText(getApplicationContext(), "orientation 270 " + orientation, Toast.LENGTH_LONG).show();
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    Log.e(TAG, " orientation normal " + orientation);
                  //  Toast.makeText(getApplicationContext(), "orientation 0 " + orientation, Toast.LENGTH_LONG).show();

                default:
                    if (IsFontCameraOpened) {
                        rotatedBitmap = rotateImage(bitmap, 180);
                    } else {
                        rotatedBitmap = bitmap;
                    }
                    Log.e(TAG, " orientation not match " + orientation);
                   // Toast.makeText(getApplicationContext(), "orientation not match " + orientation, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e(TAG, " orientation exception " + e.toString());
            Toast.makeText(getApplicationContext(), "orientation " + e.toString(), Toast.LENGTH_LONG).show();
        }
        return rotatedBitmap;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Log.e(TAG, " angle " + angle);
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void sendToImageCorrection() {
        ll_correctionTool.setVisibility(View.VISIBLE);
        ll_seekbar.setVisibility(View.VISIBLE);
        textureView.setVisibility(View.GONE);
        //ll_selected.setVisibility(View.GONE);
        ib_switch_camera.setVisibility(View.GONE);
        // takePictureButton.setVisibility(View.GONE);
        ll_takePicture.setVisibility(View.GONE);

        tv_title.setText("Adjust");

        next.setEnabled(true);
        IsImageCorrectionToolSet = true;
    }


    static Bitmap finalBitmap;
    Boolean myImageSelected = true;
    CustomSeekBar seekbar_saturation, seekBar_bright;
    HorizontalScrollView gg_scrollView, scrollView;
    CustomSeekBar gg_seekbar_saturation, gg_seekBar_bright;
    SeekBar seekbar_contrast, seekbar_temperature, seekbar_gamma, seekBar_hue;
    SeekBar gg_seekbar_contrast, gg_seekbar_temperature, gg_seekbar_gamma, gg_seekBar_hue;
    ImageButton btn_bright, btn_contrast, btn_saturation, btn_temp, btn_gamma, btn_hue, btn_adjust, gg_btn_bright, gg_btn_contrast, gg_btn_saturation, gg_btn_temp, gg_btn_gamma, gg_btn_hue, gg_btn_adjust;
    Button btn_gautiPhoto, btn_myphoto;
    LinearLayout ll_selected_myphoto, ll_selectedgauti, ll_correctionTool, ll_seekbar, ll_mySeekbar, ll_ggSeekbar;
    int brightness, saturation, hue, contrast, temperature = 10;
    int gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature = 10;
    Boolean IsAdjustClicked = true;
    float gamma = 1;
    float gg_gamma = 1;

    /**
     * below function set the view of correction tools like brightness for both images (gauti & my image)
     */

    private void setImageCorrection() {

        ll_correctionTool = (LinearLayout) findViewById(R.id.ll_correctionTool);
        ll_seekbar = (LinearLayout) findViewById(R.id.ll_seekbar);
        ll_mySeekbar = (LinearLayout) findViewById(R.id.ll_mySeekbar);
        ll_ggSeekbar = (LinearLayout) findViewById(R.id.ll_ggSeekbar);
        myImage = (ImageView) findViewById(R.id.myImage);
        btnClickedView = (View)findViewById(R.id.btn_adjust) ;
        gg_scrollView = (HorizontalScrollView) findViewById(R.id.gg_scrollView);
        scrollView = (HorizontalScrollView) findViewById(R.id.scrollView);

        seekBar_bright = (CustomSeekBar) findViewById(R.id.seekBar_bright);
        seekbar_contrast = (SeekBar) findViewById(R.id.seekbar_contrast);
        seekbar_saturation = (CustomSeekBar) findViewById(R.id.seekbar_saturation);
        seekbar_temperature = (SeekBar) findViewById(R.id.seekbar_temperature);
        seekbar_gamma = (SeekBar) findViewById(R.id.seekbar_gamma);
        seekBar_hue = (SeekBar) findViewById(R.id.seekBar_hue);

        gg_seekBar_bright = (CustomSeekBar) findViewById(R.id.gg_seekBar_bright);
        gg_seekbar_contrast = (SeekBar) findViewById(R.id.gg_seekbar_contrast);
        gg_seekbar_saturation = (CustomSeekBar) findViewById(R.id.gg_seekbar_saturation);
        gg_seekbar_temperature = (SeekBar) findViewById(R.id.gg_seekbar_temperature);
        gg_seekbar_gamma = (SeekBar) findViewById(R.id.gg_seekbar_gamma);
        gg_seekBar_hue = (SeekBar) findViewById(R.id.gg_seekBar_hue);

        btn_bright = (ImageButton) findViewById(R.id.btn_bright);
        btn_contrast = (ImageButton) findViewById(R.id.btn_contrast);
        btn_saturation = (ImageButton) findViewById(R.id.btn_saturation);
        btn_temp = (ImageButton) findViewById(R.id.btn_temp);
        btn_gamma = (ImageButton) findViewById(R.id.btn_gamma);
        btn_hue = (ImageButton) findViewById(R.id.btn_hue);
        btn_adjust = (ImageButton) findViewById(R.id.btn_adjust);

        gg_btn_bright = (ImageButton) findViewById(R.id.gg_btn_bright);
        gg_btn_contrast = (ImageButton) findViewById(R.id.gg_btn_contrast);
        gg_btn_saturation = (ImageButton) findViewById(R.id.gg_btn_saturation);
        gg_btn_temp = (ImageButton) findViewById(R.id.gg_btn_temp);
        gg_btn_gamma = (ImageButton) findViewById(R.id.gg_btn_gamma);
        gg_btn_hue = (ImageButton) findViewById(R.id.gg_btn_hue);
        gg_btn_adjust = (ImageButton) findViewById(R.id.gg_btn_adjust);
        // next = (ImageButton) findViewById(R.id.next);

        btn_gautiPhoto = (Button) findViewById(R.id.btn_gautiPhoto);
        btn_gautiPhoto.setTypeface(CustomFont.getAppFont(this));

        btn_myphoto = (Button) findViewById(R.id.btn_myphoto);
        btn_myphoto.setTypeface(CustomFont.getAppFont(this));
        ll_selected_myphoto = (LinearLayout) findViewById(R.id.ll_selected_myphoto);
        ll_selectedgauti = (LinearLayout) findViewById(R.id.ll_selectedgauti);

        LinearLayout.LayoutParams par = new LinearLayout.LayoutParams(pixWidth, (int) (pixHeight * 0.35));
        ll_correctionTool.setLayoutParams(par);


        seekBar_bright.setMax(512);
        seekBar_bright.setProgress(255);
        seekBar_bright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress - 255;
                brightness = progress;


                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        gg_seekBar_bright.setMax(512);
        gg_seekBar_bright.setProgress(255);
        gg_seekBar_bright.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress - 255;
                gg_brightness = progress;


                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_saturation.setMax(512);
        seekbar_saturation.setProgress(255);
        seekbar_saturation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress - 255;
                float prog = progress;

                saturation = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        gg_seekbar_saturation.setMax(512);
        gg_seekbar_saturation.setProgress(255);
        gg_seekbar_saturation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress - 255;
                float prog = progress;

                gg_saturation = progress;

                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_contrast.setMax(100);
        seekbar_contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                contrast = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        gg_seekbar_contrast.setMax(100);
        gg_seekbar_contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                gg_contrast = progress;

                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekbar_temperature.setMax(200);
        seekbar_temperature.setProgress(100);
        seekbar_temperature.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress / 10;
                temperature = progress;
                if (progress == 20) {
                    temperature = 19;
                }

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        gg_seekbar_temperature.setMax(200);
        gg_seekbar_temperature.setProgress(100);
        gg_seekbar_temperature.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress / 10;
                gg_temperature = progress;
                if (progress == 20) {
                    gg_temperature = 19;
                }

                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_gamma.setMax(1000);
        seekbar_gamma.setProgress(500);
        seekbar_gamma.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float prog = progress * 0.002f;
                gamma = prog;
                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        gg_seekbar_gamma.setMax(1000);
        gg_seekbar_gamma.setProgress(500);
        gg_seekbar_gamma.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float prog = progress * 0.002f;
                gg_gamma = prog;
                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBar_hue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                hue = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature, gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        gg_seekBar_hue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gg_hue = progress;

                ggImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(gg_brightness, gg_saturation, gg_hue, gg_contrast, gg_temperature, gg_gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_seekbar.setVisibility(View.GONE);
                View rootView = getWindow().getDecorView().findViewById(R.id.rl_top);

                finalBitmap = getScreenShot(rootView);//ColorFilter2.getFinalBitmap(((BitmapDrawable) myImage.getDrawable()).getBitmap());
                Intent intent = new Intent(CameraActivity.this, ApplyFilterOnImageActivity.class);
                startActivity(intent);
                ll_seekbar.setVisibility(View.VISIBLE);
                //  finish();
                //  MainActivityCamera.finalBitmapFromCamera.recycle(); MainActivityCamera.finalBitmapFromCamera = null;

            }
        });
        ll_mySeekbar.setVisibility(View.GONE);
        ll_ggSeekbar.setVisibility(View.GONE);
        btn_adjust.setImageResource(R.drawable.adjust_yellow);
        gg_btn_adjust.setImageResource(R.drawable.adjust_yellow);
        /*
        btn_adjust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myImage.setOnTouchListener(new MultiTouchListener());
                ggImage.setOnTouchListener(null);

                ll_mySeekbar.setVisibility(View.GONE);
                ll_ggSeekbar.setVisibility(View.GONE);
                btn_adjust.setImageResource(R.drawable.adjust_yellow);
                IsAdjustClicked = true;
            }
        });


        gg_btn_adjust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myImage.setOnTouchListener(null);
                ggImage.setOnTouchListener(new MultiTouchListener());

                ll_mySeekbar.setVisibility(View.GONE);
                ll_ggSeekbar.setVisibility(View.GONE);
                gg_btn_adjust.setImageResource(R.drawable.adjust_yellow);
                IsAdjustClicked = true;
            }
        });
        */


        btn_gautiPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_selected_myphoto.setBackgroundColor(Color.parseColor("#adadad"));
                ll_selectedgauti.setBackgroundColor(Color.parseColor("#f4b500"));

                myImageSelected = false;
                gg_scrollView.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                ll_mySeekbar.setVisibility(View.VISIBLE);
                ll_ggSeekbar.setVisibility(View.GONE);

               setSeekValue(btnClickedView);

            }
        });
        btn_myphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_selected_myphoto.setBackgroundColor(Color.parseColor("#f4b500"));
                ll_selectedgauti.setBackgroundColor(Color.parseColor("#adadad"));

                myImageSelected = true;
                gg_scrollView.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                ll_mySeekbar.setVisibility(View.VISIBLE);
                ll_ggSeekbar.setVisibility(View.GONE);
                setSeekValue(btnClickedView);
            }
        });
    }


    /***
     * below function is used to control the seek bar of gauti images
     * @param view // on 11-Oct this function is not used can remove
     */
    public void ggSetSeekValue(View view) {

        ll_mySeekbar.setVisibility(View.GONE);
        ll_ggSeekbar.setVisibility(View.VISIBLE);
        gg_seekBar_bright.setVisibility(View.GONE);
        gg_seekbar_contrast.setVisibility(View.GONE);
        gg_seekbar_saturation.setVisibility(View.GONE);
        gg_seekbar_temperature.setVisibility(View.GONE);
        gg_seekbar_gamma.setVisibility(View.GONE);
        gg_seekBar_hue.setVisibility(View.GONE);

        gg_btn_bright.setImageResource(R.drawable.exposure_gray);
        gg_btn_contrast.setImageResource(R.drawable.contrast_gray);
        gg_btn_saturation.setImageResource(R.drawable.saturation_gray);
        gg_btn_temp.setImageResource(R.drawable.temprature_gray);
        gg_btn_gamma.setImageResource(R.drawable.exposure_gray);
        gg_btn_hue.setImageResource(R.drawable.contrast_gray);
        gg_btn_adjust.setImageResource(R.drawable.adjust_gray);

        myImage.setOnTouchListener(null);
        ggImage.setOnTouchListener(null);
        IsAdjustClicked = false;

        if (myImageSelected) {
            ll_mySeekbar.setVisibility(View.VISIBLE);
            ll_ggSeekbar.setVisibility(View.GONE);
        } else {
            ll_mySeekbar.setVisibility(View.GONE);
            ll_ggSeekbar.setVisibility(View.VISIBLE);
        }

        switch (view.getId()) {
            case R.id.gg_btn_bright: {

                gg_seekBar_bright.setVisibility(View.VISIBLE);
                gg_btn_bright.setImageResource(R.drawable.exposure_yellow);

                break;
            }
            case R.id.gg_btn_contrast: {

                gg_seekbar_contrast.setVisibility(View.VISIBLE);
                gg_btn_contrast.setImageResource(R.drawable.contrast_yellow);
                break;
            }
            case R.id.gg_btn_saturation: {

                gg_seekbar_saturation.setVisibility(View.VISIBLE);
                gg_btn_saturation.setImageResource(R.drawable.saturation_yellow);
                break;
            }
            case R.id.gg_btn_temp: {


                gg_seekbar_temperature.setVisibility(View.VISIBLE);
                gg_btn_temp.setImageResource(R.drawable.temrature_yellow);
                break;
            }
            case R.id.gg_btn_gamma: {

                gg_seekbar_gamma.setVisibility(View.VISIBLE);
                gg_btn_gamma.setImageResource(R.drawable.exposure_yellow);
                break;
            }
            case R.id.gg_btn_hue: {


                gg_seekBar_hue.setVisibility(View.VISIBLE);
                gg_btn_hue.setImageResource(R.drawable.contrast_yellow);
                break;
            }

            case R.id.gg_btn_adjust: {
                myImage.setOnTouchListener(null);
                ggImage.setOnTouchListener(new MultiTouchListener());

                ll_mySeekbar.setVisibility(View.GONE);
                ll_ggSeekbar.setVisibility(View.GONE);
                gg_btn_adjust.setImageResource(R.drawable.adjust_yellow);
                IsAdjustClicked = true;
                break;
            }

        }


    }

    /**
     * this function is control the seekbar of my images
     *
     * @param view
     */
    View  btnClickedView;
    public void setSeekValue(View view) {
        ll_mySeekbar.setVisibility(View.GONE);
        ll_ggSeekbar.setVisibility(View.GONE);
        seekBar_bright.setVisibility(View.GONE);
        seekbar_contrast.setVisibility(View.GONE);
        seekbar_saturation.setVisibility(View.GONE);
        seekbar_temperature.setVisibility(View.GONE);
        seekbar_gamma.setVisibility(View.GONE);
        seekBar_hue.setVisibility(View.GONE);

        gg_seekBar_bright.setVisibility(View.GONE);
        gg_seekbar_contrast.setVisibility(View.GONE);
        gg_seekbar_saturation.setVisibility(View.GONE);
        gg_seekbar_temperature.setVisibility(View.GONE);
        gg_seekbar_gamma.setVisibility(View.GONE);
        gg_seekBar_hue.setVisibility(View.GONE);


        btn_bright.setImageResource(R.drawable.exposure_gray);
        btn_contrast.setImageResource(R.drawable.contrast_gray);
        btn_saturation.setImageResource(R.drawable.saturation_gray);
        btn_temp.setImageResource(R.drawable.temprature_gray);
        btn_gamma.setImageResource(R.drawable.exposure_gray);
        btn_hue.setImageResource(R.drawable.contrast_gray);
        btn_adjust.setImageResource(R.drawable.adjust_gray);


        myImage.setOnTouchListener(null);
        ggImage.setOnTouchListener(null);
        IsAdjustClicked = false;



        switch (view.getId()) {
            case R.id.btn_bright: {
                btnClickedView = findViewById(R.id.btn_bright);
                btn_bright.setImageResource(R.drawable.exposure_yellow);

                if (myImageSelected) {
                    seekBar_bright.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekBar_bright.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }


                break;
            }
            case R.id.btn_contrast: {

                btnClickedView = findViewById(R.id.btn_contrast);
                btn_contrast.setImageResource(R.drawable.contrast_yellow);
                if (myImageSelected) {
                    seekbar_contrast.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekbar_contrast.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }

                break;
            }
            case R.id.btn_saturation: {

                btnClickedView = findViewById(R.id.btn_saturation);
                btn_saturation.setImageResource(R.drawable.saturation_yellow);

                if (myImageSelected) {
                    seekbar_saturation.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekbar_saturation.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.btn_temp: {


                btnClickedView = findViewById(R.id.btn_temp);
                btn_temp.setImageResource(R.drawable.temrature_yellow);

                if (myImageSelected) {
                    seekbar_temperature.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekbar_temperature.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.btn_gamma: {

                btnClickedView = findViewById(R.id.btn_gamma);
                btn_gamma.setImageResource(R.drawable.exposure_yellow);
                if (myImageSelected) {
                    seekbar_gamma.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekbar_gamma.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }
                break;
            }
            case R.id.btn_hue: {


                btnClickedView = findViewById(R.id.btn_hue);
                btn_hue.setImageResource(R.drawable.contrast_yellow);
                if (myImageSelected) {
                    seekBar_hue.setVisibility(View.VISIBLE);
                    ll_mySeekbar.setVisibility(View.VISIBLE);
                } else {
                    gg_seekBar_hue.setVisibility(View.VISIBLE);
                    ll_ggSeekbar.setVisibility(View.VISIBLE);
                }
                break;
            }

            case R.id.btn_adjust: {

                btnClickedView = findViewById(R.id.btn_adjust);
                if (myImageSelected) {
                    myImage.setOnTouchListener(new MultiTouchListener());
                } else {
                    ggImage.setOnTouchListener(new MultiTouchListener());
                }
                ll_mySeekbar.setVisibility(View.GONE);
                ll_ggSeekbar.setVisibility(View.GONE);
                btn_adjust.setImageResource(R.drawable.adjust_yellow);
                IsAdjustClicked = true;
                break;
            }

        }

    }

    private void setAspectRatioTextureView(int ResolutionWidth, int ResolutionHeight) {
        if (ResolutionWidth > ResolutionHeight) {
            int newWidth = pixWidth;
            int newHeight = ((pixWidth * ResolutionWidth) / ResolutionHeight);
            updateTextureViewSize(newWidth, newHeight);

        } else {
            int newWidth = pixWidth;
            int newHeight = ((pixWidth * ResolutionHeight) / ResolutionWidth);
            updateTextureViewSize(newWidth, newHeight);
        }

    }

    private void updateTextureViewSize(int viewWidth, int viewHeight) {
        Log.d(TAG, "TextureView Width : " + viewWidth + " TextureView Height : " + viewHeight);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(viewWidth, viewHeight);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        textureView.setLayoutParams(params);

        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(viewWidth, viewHeight);
        param.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        myImage.setLayoutParams(param);


    }

    /***
     *  take screen shot of gauti image with my image
     * @param view
     * @return
     */
    private Bitmap getScreenShot(View view) {
        View screenView = view;
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, 0, toolbar.getHeight(), bitmap.getWidth(), (bitmap.getHeight() - (int) (pixHeight * 0.35) - toolbar.getHeight()));
        screenView.setDrawingCacheEnabled(false);
        return croppedBitmap;
    }

    private void adjustAspectRatio(int videoWidth, int videoHeight) {
        int viewWidth = textureView.getWidth();
        int viewHeight = textureView.getHeight();
        double aspectRatio = (double) videoHeight / videoWidth;

        int newWidth, newHeight;
        if (viewHeight > (int) (viewWidth * aspectRatio)) {
            // limited by narrow width; restrict height
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * aspectRatio);
        } else {
            // limited by short height; restrict width
            newWidth = (int) (viewHeight / aspectRatio);
            newHeight = viewHeight;
        }
        int xoff = (viewWidth - newWidth) / 2;
        int yoff = (viewHeight - newHeight) / 2;
        Log.e(TAG, "video=" + videoWidth + "x" + videoHeight +
                " view=" + viewWidth + "x" + viewHeight +
                " newView=" + newWidth + "x" + newHeight +
                " off=" + xoff + "," + yoff);

        Matrix txform = new Matrix();
        textureView.getTransform(txform);
        txform.setScale((float) newWidth / viewWidth, (float) newHeight / viewHeight);
        //txform.postRotate(10);          // just for fun
        txform.postTranslate(xoff, yoff);
        textureView.setTransform(txform);
    }
}