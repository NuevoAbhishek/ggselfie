package com.nuevothoughts.ggselfie;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.ConnectionDetector;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell1 on 16-Oct-17.
 */

public class ContactUsActivity extends AppCompatActivity {

    EditText ed_name, ed_email, ed_subject, ed_message;
    Button btn_send; ImageButton btn_back;
    String TAG = "ContactUs";
    ProgressBar progressBar;
    TextView screenTitle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);

        ed_name = (EditText) findViewById(R.id.ed_name);
        ed_email = (EditText) findViewById(R.id.ed_email);
        ed_subject = (EditText) findViewById(R.id.ed_subject);
        ed_message = (EditText) findViewById(R.id.ed_message);
        btn_send = (Button) findViewById(R.id.btn_send);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        screenTitle = (TextView) findViewById(R.id.screenTitle);

        screenTitle.setTypeface(CustomFont.getAppFont(this));
        ed_name.setTypeface(CustomFont.getAppFont(this));
        ed_email.setTypeface(CustomFont.getAppFont(this));
        ed_subject.setTypeface(CustomFont.getAppFont(this));
        ed_message.setTypeface(CustomFont.getAppFont(this));
        btn_send.setTypeface(CustomFont.getAppFont(this));


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ed_name.getText()!=null? ed_name.getText().toString():"";
                String email = ed_email.getText()!=null? ed_email.getText().toString():"";

                String sub = ed_subject.getText()!=null?ed_subject.getText().toString():"";
                String message = ed_message.getText()!=null?ed_message.getText().toString():"";

                if(name.length()>0&&email.length()>0&&sub.length()>0&& message.length()>0 && ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
                    callApi(name, email, sub, message);
                }else {
                    Toast.makeText(getApplicationContext(),"Some fields are empty ",Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Constant.IsUserLoggedIn) {

            ed_name.setText(Constant.NAME);
            ed_email.setText(Constant.EMAIL);

        }

    }

    private void callApi(String name, String email, String sub, String msg) {
        progressBar.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<>();
        params.put("name", name);
        params.put("email", email);
        params.put("subject", sub);
        params.put("message", msg);
        Log.e(TAG, " param " + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "contacts", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                progressBar.setVisibility(View.GONE);

                try {
                    ed_subject.setText("");
                    ed_message.setText("");

                    Toast.makeText(getApplicationContext(),"Message sent successfully",Toast.LENGTH_SHORT).show();


                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString() );
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Oops! try again later ",Toast.LENGTH_SHORT).show();


                if (error != null && error.networkResponse != null) {
                    Log.e(TAG, "Error " + error.networkResponse.statusCode);
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue



    }
}
