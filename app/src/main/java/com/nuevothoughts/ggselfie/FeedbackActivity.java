package com.nuevothoughts.ggselfie;

import android.icu.text.UnicodeSetSpanner;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell1 on 16-Oct-17.
 */

public class FeedbackActivity extends AppCompatActivity {
    String TAG = "Feedback";
    ImageButton btn_back;
    EditText ed_write,ed_email;
    Button btn_send;
    ProgressBar progressBar;
    TextView screenTitle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_feedback);

        screenTitle = (TextView)findViewById(R.id.screenTitle);

        btn_back = (ImageButton) findViewById(R.id.btn_back);
        ed_write = (EditText) findViewById(R.id.ed_write);
        ed_email = (EditText) findViewById(R.id.ed_email);
        btn_send = (Button) findViewById(R.id.btn_send);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        screenTitle.setTypeface(CustomFont.getAppFont(this));
        ed_write.setTypeface(CustomFont.getAppFont(this));
        ed_email.setTypeface(CustomFont.getAppFont(this));
        btn_send.setTypeface(CustomFont.getAppFont(this));




        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = ed_email.getText()!=null?ed_email.getText().toString():"";
                String feedback = ed_write.getText()!=null?ed_write.getText().toString():"";
                if (email.length()> 0 && feedback.length()>0) {

                    sendFeedback(email,feedback);

                }else {
                    Toast.makeText(getApplication(),"Some fields are empty",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Constant.IsUserLoggedIn) {


            ed_email.setText(Constant.EMAIL);

        }
    }



    private void sendFeedback(String email, String feedback) {


        Map<String, String> params = new HashMap<>();

        params.put("email", email);
        params.put("feedback", feedback);
        Log.e(TAG, " param " + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "feedbacks", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                progressBar.setVisibility(View.GONE);

                try {

                    ed_write.setText("");

                    Toast.makeText(getApplicationContext(),"Thanks for your feedback",Toast.LENGTH_SHORT).show();


                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString() );
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"Oops! try again later ",Toast.LENGTH_SHORT).show();


                if (error != null && error.networkResponse != null) {
                    Log.e(TAG, "Error " + error.networkResponse.statusCode);
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue






    }
}
