package com.nuevothoughts.ggselfie;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.github.clans.fab.FloatingActionButton;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Helper.MyBounceInterpolator;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.Service.EnterInContestService;
import com.nuevothoughts.ggselfie.Utility.AndroidMultiPartEntity;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by dell1 on 28-Sep-17.
 */

public class FinishActivity extends AppCompatActivity {
    String TAG = "FinishActivity";
    File storedImagePath = null;
    RelativeLayout rl_imageView, rl_bottom;
    ImageView imageView;
    String contestTitle, contestDescription, endTime;
    ImageButton btn_share, btn_save, btn_back, btn_menu;
    FloatingActionButton fab_share_friends, fab_contest;
    LinearLayout ll_contest, ll_share_friends, ll_participate, ll_share_with_friends;
    ProgressDialog progressDialog;
    Boolean imageUploadSuccess = false;
    RelativeLayout rl_contestInfo;
    Button btn_participate;
    ImageButton btn_close;
    TextView contest_description,contest_title,tv_title,tv_contest,tv_share;
    BroadcastReceiver enterInContest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            Boolean success = intent.getBooleanExtra("Success", false);

            if (success) {

                Intent intent1 = new Intent(context,GGSocialMediaActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                intent1.putExtra("openedFrom","FinishActivity");
                startActivity(intent1);
                finish();

            } else {

                Toast.makeText(getApplicationContext(), "Oops! try again later", Toast.LENGTH_SHORT).show();
            }

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        rl_imageView = (RelativeLayout) findViewById(R.id.rl_imageView);
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);
        imageView = (ImageView) findViewById(R.id.imageView);
        btn_share = (ImageButton) findViewById(R.id.btn_share);
        btn_save = (ImageButton) findViewById(R.id.btn_save);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_menu = (ImageButton) findViewById(R.id.btn_menu);
        btn_close = (ImageButton) findViewById(R.id.btn_close);
        fab_share_friends = (FloatingActionButton) findViewById(R.id.fab_share_friends);
        fab_contest = (FloatingActionButton) findViewById(R.id.fab_contest);
        rl_contestInfo = (RelativeLayout) findViewById(R.id.rl_contestInfo);
        btn_participate = (Button) findViewById(R.id.btn_participate);
        btn_participate.setTypeface(CustomFont.getAppFont(this));
        contest_description = (TextView) findViewById(R.id.contest_description);
        contest_description.setTypeface(CustomFont.getAppFont(this));

        tv_share = (TextView) findViewById(R.id.tv_share);
        tv_share.setTypeface(CustomFont.getAppFont(this));
        tv_contest = (TextView) findViewById(R.id.tv_contest);
        tv_contest.setTypeface(CustomFont.getAppFont(this));
        contest_title = (TextView) findViewById(R.id.contest_title);
        contest_title.setTypeface(CustomFont.getAppFont(this));
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setTypeface(CustomFont.getAppFont(this));
        ll_contest = (LinearLayout) findViewById(R.id.ll_contest);
        ll_share_friends = (LinearLayout) findViewById(R.id.ll_share_friends);
        ll_participate = (LinearLayout) findViewById(R.id.ll_participate);
        ll_share_with_friends = (LinearLayout) findViewById(R.id.ll_share_with_friends);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(displayMetrics.widthPixels, (int) (displayMetrics.heightPixels * 0.35));
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        rl_bottom.setLayoutParams(layoutParams);

        imageView.setImageBitmap(ApplyFilterOnImageActivity.finalBitmapAfterFilter);

        ll_contest.setVisibility(View.GONE);
        ll_share_friends.setVisibility(View.GONE);
        ll_participate.setVisibility(View.GONE);
        ll_share_with_friends.setVisibility(View.GONE);
        progressDialog = new ProgressDialog(this);
        final Animation myAnim = AnimationUtils.loadAnimation(FinishActivity.this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                storeImage(((BitmapDrawable) imageView.getDrawable()).getBitmap(),true);

            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_contestInfo.setVisibility(View.GONE);
            }
        });

        btn_participate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String socialId = new SavedData(getApplicationContext()).getSocial_id();
                String socialType = new SavedData(getApplicationContext()).getSocial_type();

                if (socialId == null || socialType == null) {
                    // if null then open activity to login
                    Intent intent = new Intent(FinishActivity.this, LoginActivity2.class);
                    intent.putExtra("openedFrom", "FinishActivity");
                    intent.putExtra("storedImagePath", storedImagePath.toString());
                    Log.e("Path ", storedImagePath.toString());
                    startActivity(intent);
                } else if (socialId != null && socialType != null && !Constant.IsUserLoggedIn) {

                    checkLogin(socialId, socialType);
                } else if (Constant.IsUserLoggedIn) {//  firstly check user is logged in
                    if (storedImagePath != null) {


                        getActiveContest();

                    }
                }
            }
        });

        fab_contest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_participate.setEnabled(false);
                getActiveContestBefore();


            }
        });

        fab_share_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (storedImagePath != null) {
                    File f = new File(storedImagePath.toString());
                    Uri uri = Uri.parse("file://" + f.getAbsolutePath());
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    share.setType("image/*");
                    share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(Intent.createChooser(share, "Share image"));

                }

            }
        });

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (storedImagePath == null) {
                    storeImage(((BitmapDrawable) imageView.getDrawable()).getBitmap(),false);
                }

                ll_contest.setVisibility(View.VISIBLE);
                ll_share_friends.setVisibility(View.VISIBLE);
                ll_participate.setVisibility(View.VISIBLE);
                ll_share_with_friends.setVisibility(View.VISIBLE);

                ll_contest.startAnimation(myAnim);
                ll_share_friends.startAnimation(myAnim);
                ll_participate.startAnimation(myAnim);
                ll_share_with_friends.startAnimation(myAnim);


            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FinishActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

            }
        });
    }

    private void storeImage(Bitmap image,boolean IsShowMessage) {
        File pictureFile = getOutputMediaFile();

        if (pictureFile == null) {
            Log.d(TAG, "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        storedImagePath = pictureFile;
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            if(IsShowMessage) {
                Toast.makeText(getApplicationContext(), "Image Saved Successfully", Toast.LENGTH_LONG).show();
            }
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (storedImagePath == null) {
            storeImage(((BitmapDrawable) imageView.getDrawable()).getBitmap(),false);
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(enterInContest,
                new IntentFilter(Constant.ContestBroadcastName)
        );

    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(enterInContest);

    }

    /**
     * get the live contest
     */
    private void getActiveContest() {
        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("fetching live contest...");
        progressDialog.show();
        //now create the object of the custom class
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "contests/getAllActiveContests", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {



                    JSONArray jsonArray = response.getJSONArray("contest");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        jsonArray.getJSONObject(i).getString("contest_id");
                        contestTitle = jsonArray.getJSONObject(i).getString("title");
                        contestDescription = jsonArray.getJSONObject(i).getString("description");
                        jsonArray.getJSONObject(i).getString("contestType");
                        endTime = jsonArray.getJSONObject(i).getString("end_date");

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        String time = simpleDateFormat.format(c.getTime());
                        Log.e("SendChat ", time);

                        Date dateDB = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(time.replaceAll("Z$", "+0000"));
                        long curTime = (dateDB.getTime());// this is timestamp of date saved in db

                        Date dateChat = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(endTime.replaceAll("Z$", "+0000"));
                        long endT = (dateChat.getTime());// this is the timestamp of chat coming from server

                        Log.e(TAG, " Success " + contestTitle + " " + contestDescription + " " + curTime + " " + endT);



                            Constant.CONTEST_ID = jsonArray.getJSONObject(i).getString("contest_id");

                            break;

                    }

                    progressDialog.dismiss();
                    new UploadImage().execute();

                    progressDialog.setMessage("uploading image for contest...");
                    progressDialog.show();
                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue


    }

    /**
     * make login every time user is launching the app.
     *
     * @param socialId
     * @param socialType
     */
    private void checkLogin(String socialId, String socialType) {

        progressDialog.setMessage("checking your login...");
        progressDialog.show();
        Map<String, String> params = new HashMap<>();
        params.put("social_id", socialId);
        params.put("social_type", socialType);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "GGFUsers/socialLogin", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {
                    Constant.IsUserLoggedIn = true;

                    JSONArray jsonArray = response.getJSONArray("user");

                    for (int i = 0; i < 1; i++) {
                        Constant.ID = jsonArray.getJSONObject(i).getString("id");
                        Constant.EMAIL = jsonArray.getJSONObject(i).getString("email");
                        Constant.NAME = jsonArray.getJSONObject(i).getString("name");
                        Constant.ACCESS_TOKEN = jsonArray.getJSONObject(i).getString("accessToken");

                    }
                    progressDialog.dismiss();

                    if (Constant.IsUserLoggedIn) {
                        if (storedImagePath != null) {

                            getActiveContest();
                        }
                    }

                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());


                Intent intent = new Intent(FinishActivity.this, LoginActivity2.class);
                intent.putExtra("openedFrom", "FinishActivity");
                startActivity(intent);

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue


    }


    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/GGSelfie");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public class UploadImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            uploadImage(storedImagePath.toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (imageUploadSuccess) {
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            }
        }
    }


    //function to upload the image
    private void uploadImage(String filePath) {
        //upload the data to the server
        String responseString = null;
        HttpClient httpclient = new DefaultHttpClient();


        HttpPost httpPost = new HttpPost(Constant.ImageUploadUrl);


        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener() {
                @Override
                public void transferred(long num) {
                    Log.e("Image Uploaded: ", "" + num);
                    //publishProgress((int) ((num / (float) totalSize) * 100));//publish the progress report
                }
            });

            File sourceFile = new File(filePath);

            entity.addPart("image", new FileBody(sourceFile));

            httpPost.setEntity(entity);

            //make server call
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity r_entity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {//if success
                //get the server response
                imageUploadSuccess = true;
                responseString = EntityUtils.toString(r_entity);
                Log.e("Image Upload : ", "deleted " + filePath);
                /** // Delete file
                 File fdelete = new File(filePath);
                 if (fdelete.exists()) {
                 if (fdelete.delete()) {

                 } else {

                 }
                 }
                 **/
                progressDialog.dismiss();
                Intent intent = new Intent(this, EnterInContestService.class);
                String imageName = filePath.substring(filePath.lastIndexOf("/") + 1);
                intent.putExtra("imageName", imageName);
                startService(intent);
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "something bad occurred, try again later", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

        Log.e("Image Upload Error", responseString);
        //Toast.makeText(getApplicationContext(), "Response" + responseString, Toast.LENGTH_LONG).show;
    }


    /**
     * function to convert the server time to desired format
     *
     * @param time
     * @return
     */
    private String formatTime(String time) {
        String fTime = "";

        //2016-11-09T05:50:50.431Z
        String datePart = time.substring(0, 10);
        String day = datePart.substring(8, 10);
        String month = datePart.substring(5, 7);
        String hourPart = time.substring(11, 13);
        String minPart = time.substring(14, 16);

        int int_day = Integer.parseInt(day);
        int int_month = Integer.parseInt(month);
        //for the current time
        Calendar calendar = Calendar.getInstance();
        int c_month = calendar.get(Calendar.MONTH) + 1;
        int c_day = calendar.get(Calendar.DAY_OF_MONTH);
        int c_year = calendar.get(Calendar.YEAR);


        //now check if same month and same date
        if ((int_month == c_month) && (int_day == c_day)) {//today
            if (Integer.parseInt(hourPart) >= 12) {
                fTime = Integer.parseInt(hourPart) - 12 + ":" + minPart + " PM";
            } else {
                fTime = hourPart + ":" + minPart + " AM";
            }

        } else {//not the same day

            //format the data in dd:MMM format
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                Date d = sdf.parse(datePart);

                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM");

                fTime = sdf1.format(d);
            } catch (ParseException ex) {
                // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat constructor
            }

        }
        return fTime;
    }


    /**
     * get the live contest
     */
    private void getActiveContestBefore() {
        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("fetching live contest...");
        progressDialog.show();
        //now create the object of the custom class
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "contests/getAllActiveContests", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {



                    JSONArray jsonArray = response.getJSONArray("contest");

                    for (int i = 0; i < jsonArray.length(); i++) {

                        jsonArray.getJSONObject(i).getString("contest_id");
                        contestTitle = jsonArray.getJSONObject(i).getString("title");
                        contestDescription = jsonArray.getJSONObject(i).getString("description");
                        jsonArray.getJSONObject(i).getString("contestType");
                        contest_title.setText(contestTitle);
                        contest_description.setText(contestDescription);
                        endTime = jsonArray.getJSONObject(i).getString("end_date");

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        String time = simpleDateFormat.format(c.getTime());
                        Log.e("SendChat ", time);

                        Date dateDB = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(time.replaceAll("Z$", "+0000"));
                        long curTime = (dateDB.getTime());// this is timestamp of date saved in db

                        Date dateChat = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(endTime.replaceAll("Z$", "+0000"));
                        long endT = (dateChat.getTime());// this is the timestamp of chat coming from server

                        Log.e(TAG, " Success " + contestTitle + " " + contestDescription + " " + curTime + " " + endT);



                        Constant.CONTEST_ID = jsonArray.getJSONObject(i).getString("contest_id");

                        break;

                    }
                    rl_contestInfo.setVisibility(View.VISIBLE);
                    btn_participate.setEnabled(true);
                    progressDialog.dismiss();

                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue


    }

}
