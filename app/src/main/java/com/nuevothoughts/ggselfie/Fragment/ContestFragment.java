package com.nuevothoughts.ggselfie.Fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.nuevothoughts.ggselfie.Adapter.AdapterContest;
import com.nuevothoughts.ggselfie.ApplyFilterOnImageActivity;
import com.nuevothoughts.ggselfie.GGSocialMediaActivity;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.ContestEvent;
import com.nuevothoughts.ggselfie.LoginActivity2;
import com.nuevothoughts.ggselfie.Model.ModelForContest;
import com.nuevothoughts.ggselfie.R;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dell1 on 12-Oct-17.
 */

public class ContestFragment extends Fragment implements ContestEvent {

    RecyclerView recyclerView;
    String TAG = "ContestFragment";
    AdapterContest adapterContest;
    List<ModelForContest> contestList = new ArrayList<ModelForContest>();
    ProgressBar progressBar;
    int positionForWhichLikeDislike;
    SwipyRefreshLayout swipyrefreshlayout;
    String lastDate;
    boolean IsCalled = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contest, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        swipyrefreshlayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipyrefreshlayout);


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        adapterContest = new AdapterContest(getApplicationContext(), contestList, this);
        recyclerView.setAdapter(adapterContest);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection swipyRefreshLayoutDirection) {
                swipyrefreshlayout.setRefreshing(false);
                if(Constant.IsUserLoggedIn && !IsCalled) {
                    getMoreSelfieOnRefreshWithId(lastDate);
                }else {

                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        if (Constant.IsUserLoggedIn) {
            getContestSelfies();

        } else {
            getContestSelfieWithoutUserId();// when open directly from home screen

        }

    }

    @Override
    public void onPause() {
        super.onPause();


    }


    private void getMoreSelfieOnRefreshWithId(String date) {
        Log.e(TAG, " last date  " + date + "id "+Constant.ID);
        IsCalled = true; // now true so no call again until it become false
        Map<String, String> params = new HashMap<>();
        params.put("date", date);
        try {
             date = URLEncoder.encode(date, "utf-8");
        }catch (Exception e)
        {

        }

        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "selfies/getSelfie?user_id="+Constant.ID+"&date="+date, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                Log.e(TAG, "Success On refresh having id " + response);
               // progressBar.setVisibility(View.GONE);
                //contestList.clear();
                try {
                    JSONArray jsonArray = response.getJSONArray("selfie");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        ModelForContest modelForContest = new ModelForContest();

                        modelForContest.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                        modelForContest.setSelfieId(jsonArray.getJSONObject(i).getString("selfie_id"));
                        modelForContest.setSelfieDate(formatTime(jsonArray.getJSONObject(i).getString("selfieDate")));
                        lastDate = jsonArray.getJSONObject(i).getString("selfieDate");
                        modelForContest.setName(jsonArray.getJSONObject(i).getString("name"));
                        modelForContest.setTotalLikes(jsonArray.getJSONObject(i).getString("total_likes").equals("null") ? "0" : jsonArray.getJSONObject(i).getString("total_likes"));

                        modelForContest.setUserLiked(jsonArray.getJSONObject(i).getString("isliked").equals("null") ? "0" : "1");
                        modelForContest.setImageName(jsonArray.getJSONObject(i).getString("url"));

                        Log.e(TAG, "Image name " + jsonArray.getJSONObject(i).getString("url"));
                        modelForContest.setCallWithUserId(true);
                        contestList.add(modelForContest);


                    }

                    adapterContest.notifyDataSetChanged();
                    swipyrefreshlayout.setRefreshing(false);
                    IsCalled = false;
                } catch (Exception e) {
                    Log.e(TAG, "Exception " + e.toString());


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "No more selfie found, try again later ", Toast.LENGTH_SHORT).show();
                swipyrefreshlayout.setRefreshing(false);
                IsCalled = false;
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "getMoreSelfiesWithId");//add the request to the queue



    }

    public void getContestSelfies() {

        progressBar.setVisibility(View.VISIBLE);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "selfies/getSelfie?user_id=" + Constant.ID, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, "Success " + response);
                progressBar.setVisibility(View.GONE);
                contestList.clear();
                try {
                    JSONArray jsonArray = response.getJSONArray("selfie");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        ModelForContest modelForContest = new ModelForContest();

                        modelForContest.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                        modelForContest.setSelfieId(jsonArray.getJSONObject(i).getString("selfie_id"));
                        modelForContest.setSelfieDate(formatTime(jsonArray.getJSONObject(i).getString("selfieDate")));
                        lastDate = jsonArray.getJSONObject(i).getString("selfieDate");
                        modelForContest.setName(jsonArray.getJSONObject(i).getString("name"));
                        modelForContest.setTotalLikes(jsonArray.getJSONObject(i).getString("total_likes").equals("null") ? "0" : jsonArray.getJSONObject(i).getString("total_likes"));

                        modelForContest.setUserLiked(jsonArray.getJSONObject(i).getString("isliked").equals("null") ? "0" : "1");
                        modelForContest.setImageName(jsonArray.getJSONObject(i).getString("url"));

                        Log.e(TAG, "Image name " + jsonArray.getJSONObject(i).getString("url"));
                        modelForContest.setCallWithUserId(true);
                        contestList.add(modelForContest);


                    }

                    adapterContest.notifyDataSetChanged();


                } catch (Exception e) {
                    Log.e(TAG, "Exception " + e.toString());


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "getSelfie");//add the request to the queue


    }

    private void getContestSelfieWithoutUserId() {

        progressBar.setVisibility(View.VISIBLE);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "selfies/getSelfie", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressBar.setVisibility(View.GONE);
                Log.e(TAG, "Success withoutId " + response);

                contestList.clear();
                try {
                    JSONArray jsonArray = response.getJSONArray("selfie");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        ModelForContest modelForContest = new ModelForContest();

                        modelForContest.setUserId(jsonArray.getJSONObject(i).getString("user_id"));
                        modelForContest.setSelfieId(jsonArray.getJSONObject(i).getString("selfie_id"));
                        modelForContest.setSelfieDate(formatTime(jsonArray.getJSONObject(i).getString("selfieDate")));
                        modelForContest.setName(jsonArray.getJSONObject(i).getString("name"));
                        modelForContest.setTotalLikes(jsonArray.getJSONObject(i).getString("total_likes").equals("null") ? "0" : jsonArray.getJSONObject(i).getString("total_likes"));
                        // modelForContest.setUserLiked(jsonArray.getJSONObject(i).getString("isliked"));
                        modelForContest.setImageName(jsonArray.getJSONObject(i).getString("url"));
                        modelForContest.setCallWithUserId(false);
                        Log.e(TAG, "Image name " + jsonArray.getJSONObject(i).getString("url"));
                        contestList.add(modelForContest);


                    }

                    adapterContest.notifyDataSetChanged();


                } catch (Exception e) {
                    Log.e(TAG, "Exception " + e.toString());


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "getSelfieWithoutId");//add the request to the queue


    }

    @Override
    public void likeDislikeClick(int position) {

        if (contestList.get(position).isCallWithUserId()) {// if user is already logged in
            likeDislikeServerCall(position);
            if (contestList.get(position).isUserLiked().equals("0")) {
                contestList.get(position).setUserLiked("1");// user liked this selfie
                int likes = Integer.parseInt(contestList.get(position).getTotalLikes()) + 1;
                contestList.get(position).setTotalLikes("" + likes);
            } else {// user dislike this selfie

                contestList.get(position).setUserLiked("0");// user liked this selfie
                int likes = Integer.parseInt(contestList.get(position).getTotalLikes()) - 1;
                contestList.get(position).setTotalLikes("" + likes);
            }
            adapterContest.notifyDataSetChanged();
        } else {// user is not logged in or credential not found

            Intent intent = new Intent(getApplicationContext(), LoginActivity2.class);
            intent.putExtra("openedFrom", "ContestFragment");
            startActivity(intent);
        }
    }


    private void likeDislikeServerCall(int position) {

        contestList.get(position).getSelfieId();

        Map<String, String> params = new HashMap<>();
        params.put("user_id", Constant.ID);
        params.put("selfie_id", contestList.get(position).getSelfieId());

        Log.e(TAG, " likeDislike params " + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "selfie_likes/likeUnlikeSefie?access_token=" + Constant.ACCESS_TOKEN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e(TAG, " likeDislike Success " + response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "likeDislike");//add the request to the queue


    }

    /**
     * function to convert the server time to desired format
     *
     * @param time
     * @return
     */
    private String formatTime(String time) {
        String fTime = "";

        //2016-11-09T05:50:50.431Z
        String datePart = time.substring(0, 10);
        String day = datePart.substring(8, 10);
        String month = datePart.substring(5, 7);
        String hourPart = time.substring(11, 13);
        String minPart = time.substring(14, 16);

        int int_day = Integer.parseInt(day);
        int int_month = Integer.parseInt(month);
        //for the current time
        Calendar calendar = Calendar.getInstance();
        int c_month = calendar.get(Calendar.MONTH) + 1;
        int c_day = calendar.get(Calendar.DAY_OF_MONTH);
        int c_year = calendar.get(Calendar.YEAR);


        //now check if same month and same date
        if ((int_month == c_month) && (int_day == c_day)) {//today
            if (Integer.parseInt(hourPart) >= 12) {
                fTime = Integer.parseInt(hourPart) - 12 + ":" + minPart + " PM";
            } else {
                fTime = hourPart + ":" + minPart + " AM";
            }

        } else {//not the same day

            //format the data in dd:MMM format
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // here set the pattern as you date in string was containing like date/month/year
                Date d = sdf.parse(datePart);

                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MMM");

                fTime = sdf1.format(d);
            } catch (ParseException ex) {
                // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat constructor
            }

        }
        return fTime;
    }


}
