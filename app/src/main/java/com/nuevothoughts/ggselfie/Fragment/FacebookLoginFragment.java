package com.nuevothoughts.ggselfie.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import com.nuevothoughts.ggselfie.LoginActivity2;
import com.nuevothoughts.ggselfie.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * Created by dell1 on 07-Oct-17.
 */

public class FacebookLoginFragment extends Fragment {

    String TAG = "FacebookLogin";
    Button btn_login_fb;
    CallbackManager callbackManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  rootView = inflater.inflate(R.layout.fragment_facebooklogin,container,false);

        btn_login_fb = (Button) rootView.findViewById(R.id.btn_login_fb);

        return  rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        btn_login_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loginUser();
            }
        });
    }
          /*
   Function to login the user from facebook
    */
    public void loginUser() {


        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                // GraphRequest is used to fetch the profile's information line name ,email etc.

                Log.e("FromFacebook", loginResult.toString());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        //  Log.e("FromFacebookonComplete", object.toString());
                        if (object != null) {
                            Bundle bundle = getFacebookData(object);//This is called to parse JSONObject object value and return in form of bundle
                            // checkIfExistingUser(bundle);//This function is used to send the data to home activity.
                        } else {


                        }

                        //for getting the facebook i

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,link,gender,location,birthday");
                request.setParameters(parameters);
                request.executeAsync();


                //for getting the user

            }

            @Override
            public void onCancel() {
                // App code
                Log.e("Facebook Login", "Cancelled");
                Toast.makeText(getActivity(), "You have cancelled the login process ,some times it happens if you have an external app locking the facebook app", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("Facebook Login Error", exception.getMessage());
                Toast.makeText(getActivity(), "Error while connecting with facebook please try after some time", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * function to parse the facebook login result
     *
     * @param object
     * @return
     */
    private Bundle getFacebookData(JSONObject object) {

        Log.e("getFacebookData ", "" + object.toString());
        Bundle bundle = new Bundle();

        try {
            String id = object.getString("id");
            String name = object.getString("name");
            Log.e("getFacebookData", id.toString());

            ((LoginActivity2)getActivity()).name = name;
            ((LoginActivity2)getActivity()).socialId =  id;
            ((LoginActivity2)getActivity()).socialType =  "1";


            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=9999&height=9999");
                Log.e("profile_pic", profile_pic + "");
                bundle.putString("Pic_Url1", profile_pic.toString());

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("id", id);
            bundle.putString("name", name);

            if (object.has("email")) {

                String fbemail = object.getString("email");

                //sendInfoToApi( na, fbemail, date);
                Log.e("email", fbemail);
                bundle.putString("email", object.getString("email"));
                ((LoginActivity2)getActivity()).email =  fbemail;
            } else {
                bundle.putString("email", "test@ofish.com");
            }


            if (object.has("gender")) {
                // Toast.makeText(getActivity(),object.getString("gender").toString(),Toast.LENGTH_LONG);

                bundle.putString("gender", object.getString("gender"));

                Log.e("gender", object.getString("gender").toString());
            } else {
                bundle.putString("gender", "male");
            }
            if (object.has("location")) {
                bundle.putString("location", object.getJSONObject("location").getString("name"));
                Log.e("location", object.getJSONObject("location").getString("name"));
            }
            if (object.has("birthday")) {
                bundle.putString("birthday", object.getString("birthday"));
                Log.e("birthday", object.getString("birthday"));

            }

            ((LoginActivity2)getActivity()).sendLoginDataToServer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return bundle;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            Log.e(TAG," Facebook login request code "+ requestCode);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }


}
