package com.nuevothoughts.ggselfie.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nuevothoughts.ggselfie.CameraActivity;
import com.nuevothoughts.ggselfie.FinishActivity;
import com.nuevothoughts.ggselfie.R;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by dell1 on 21-Sep-17.
 */

public class GalleryFragment extends Fragment {

    private static Uri[] mUrls = null;
    private static String[] strUrls = null;
    private String[] mNames = null;
    private GridView gridview = null;
    private Cursor cc = null;
    LinearLayout ll_selected_camera;
    LinearLayout ll_selected_gallery;
    Button btn_camera;

    private ProgressDialog myProgressDialog = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        ll_selected_camera = (LinearLayout) rootView.findViewById(R.id.ll_selected_camera);
        ll_selected_gallery = (LinearLayout) rootView.findViewById(R.id.ll_selected_gallery);
        btn_camera = (Button) rootView.findViewById(R.id.btn_camera);

        // It have to be matched with the directory in SDCard
        cc = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);

        // File[] files=f.listFiles();
        if (cc != null) {

            myProgressDialog = new ProgressDialog(getActivity());
            myProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            myProgressDialog.setMessage("hello ");
            //myProgressDialog.setIcon(R.drawable.blind);
            myProgressDialog.show();

            new Thread() {
                public void run() {
                    try {
                        cc.moveToFirst();
                        mUrls = new Uri[cc.getCount()];
                        strUrls = new String[cc.getCount()];
                        mNames = new String[cc.getCount()];
                        for (int i = 0; i < cc.getCount(); i++) {
                            cc.moveToPosition(i);
                            mUrls[i] = Uri.parse(cc.getString(1));
                            strUrls[i] = cc.getString(1);
                            mNames[i] = cc.getString(3);
                            //Log.e("mNames[i]",mNames[i]+":"+cc.getColumnCount()+ " : " +cc.getString(3));
                        }

                    } catch (Exception e) {
                    }
                    myProgressDialog.dismiss();
                }
            }.start();
            gridview = (GridView) rootView.findViewById(R.id.gridview);
            gridview.setAdapter(new ImageAdapter(getContext()));

            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {

                    Log.e("position ", " " + strUrls[position]);

                    ((CameraActivity) getActivity()).setImageFromGallery(strUrls[position]);

                    /*
                    Intent i = new Intent(GalleryFragment.this, FinishActivity.class);

                    Log.e("intent : ", ""+position);

                    i.putExtra("imgUrls", strUrls);

                    i.putExtra("position", position);

                    startActivity(i);
                    */

                }
            });

        }


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_selected_camera.setBackgroundColor(Color.parseColor("#f4b500"));
                ll_selected_gallery.setBackgroundColor(Color.parseColor("#adadad"));
                ((CameraActivity) getActivity()).closeGalleryFragment();

                ll_selected_camera.setBackgroundColor(Color.parseColor("#adadad"));
                ll_selected_gallery.setBackgroundColor(Color.parseColor("#f4b500"));
            }
        });
    }


    /**
     * This class loads the image gallery in grid view.
     */
    public class ImageAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<Bitmap> bitmapList;

        public ImageAdapter(Context context) {
            this.context = context;
            this.bitmapList = bitmapList;
        }

        public int getCount() {
            return cc.getCount();
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(this.context);
                imageView.setLayoutParams(new GridView.LayoutParams(80, 80));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (ImageView) convertView;
            }

           // Bitmap bmp = decodeURI(mUrls[position].getPath());

            Glide.with(context).load("file://" +mUrls[position].getPath()).into(imageView);
           // imageView.setImageBitmap(bmp);
            return imageView;
        }
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        // FlurryAgent.onStartSession(this, "***");
    }

    /**
     * This method is to scale down the image
     */
    public Bitmap decodeURI(String filePath) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;


        // Only scale if we need to 
        // (16384 buffer for img processing)

        Boolean scaleByHeight = Math.abs(options.outHeight - 100) >= Math.abs(options.outWidth - 100);

        /*
        if (options.outHeight * options.outWidth * 2 >= 16384) {
            // Load, scaling to smallest power of 2 that'll get it <= desired dimensions
            double sampleSize = scaleByHeight
                    ? options.outHeight / 100
                    : options.outWidth / 100;
            options.inSampleSize =
                    (int) Math.pow(2d, Math.floor(Math.log(sampleSize) / Math.log(2d)));
        }


        // Do the actual decoding
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[512];
 */
        options.inJustDecodeBounds = false;
        Bitmap output = BitmapFactory.decodeFile(filePath, options);

        return output;

    }


}
