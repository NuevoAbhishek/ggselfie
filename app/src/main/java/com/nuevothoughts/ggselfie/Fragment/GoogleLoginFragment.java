package com.nuevothoughts.ggselfie.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.internal.CallbackManagerImpl;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.LoginActivity2;
import com.nuevothoughts.ggselfie.R;

/**
 * Created by dell1 on 07-Oct-17.
 */

public class GoogleLoginFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{
    String TAG = "GoogleLogin";

    CallbackManager callbackManager;
    SignInButton btn_login_with_google;
    GoogleSignInOptions gso;

    private GoogleApiClient mGoogleApiClient;
    private ProgressDialog mProgressDialog;
    private static final int RC_SIGN_IN = 9001;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View  rootView = inflater.inflate(R.layout.fragment_googlelogin,container,false);

        btn_login_with_google = (SignInButton) rootView.findViewById(R.id.btn_login_with_google);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        btn_login_with_google.setOnClickListener(this);
        return  rootView;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {


        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    @Override
    public void onClick(View v) {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);


    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.e(TAG, "handleSignInResult:" + result.isSuccess() + " " + result.toString() + " " + result.getStatus());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
           // name.setText(acct.getDisplayName());
            //email.setText(acct.getEmail());

            ((LoginActivity2)getActivity()).name = acct.getDisplayName();
            ((LoginActivity2)getActivity()).socialId =  acct.getId();
            ((LoginActivity2)getActivity()).socialType=  "2";
            ((LoginActivity2)getActivity()).email=  acct.getEmail();
             new SavedData(getActivity()).setGoogleProfilePicUrl(acct.getPhotoUrl()!=null?acct.getPhotoUrl().toString():"null");

            ((LoginActivity2)getActivity()).sendLoginDataToServer();
            //Glide.with(getApplicationContext()).load(acct.getPhotoUrl().toString()).into(pic);
            Log.e(TAG, "result " + acct.getDisplayName());
            //  updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            // updateUI(false);
            signOut();
        }
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        // updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }

}
