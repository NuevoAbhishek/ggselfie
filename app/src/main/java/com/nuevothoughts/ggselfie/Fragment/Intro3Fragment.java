package com.nuevothoughts.ggselfie.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.nuevothoughts.ggselfie.GGChooseActivity;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.R;

/**
 * Created by dell1 on 12-Oct-17.
 */

public class Intro3Fragment extends Fragment {

    ImageButton start;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro3, container, false);

        start = (ImageButton)rootView.findViewById(R.id.start);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SavedData(getActivity()).setUserFirstTime(false);
                Intent intent = new Intent(getActivity(), GGChooseActivity.class);
                startActivity(intent);

                getActivity().finish();
            }
        });
    }
}
