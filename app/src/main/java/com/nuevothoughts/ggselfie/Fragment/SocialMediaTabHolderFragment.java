package com.nuevothoughts.ggselfie.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.design.widget.TabLayout;

import com.nuevothoughts.ggselfie.R;

/**
 * Created by dell1 on 20-Sep-17.
 */

public class SocialMediaTabHolderFragment extends Fragment {

    TabLayout tb ;ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_socialmedia_tab_holder, container, false);
        tb = (TabLayout) rootView.findViewById(R.id.tab);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);

        return rootView;
    }
}
