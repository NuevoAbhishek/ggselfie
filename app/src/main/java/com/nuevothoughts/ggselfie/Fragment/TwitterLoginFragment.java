package com.nuevothoughts.ggselfie.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.internal.CallbackManagerImpl;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.nuevothoughts.ggselfie.Helper.Constant;

import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.LoginActivity2;
import com.nuevothoughts.ggselfie.R;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import retrofit2.Call;

/**
 * Created by dell1 on 07-Oct-17.
 */

public class TwitterLoginFragment extends Fragment {

    TwitterLoginButton btn_login_twitter;
     String TAG ="TwitterLogin";
    TwitterAuthClient authClient;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_twitterlogin, container, false);



        btn_login_twitter = (TwitterLoginButton) rootView.findViewById(R.id.btn_login_twitter);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

         authClient = new TwitterAuthClient();



        btn_login_twitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                Log.e(TAG," "+result.toString());
               final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                // name.setText(session.getUserName());
                getProfileImage();


                authClient.requestEmail(session, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        // Do something with the result, which provides the email address

                       Log.e(TAG," Email Success "+ result.data);

                        ((LoginActivity2)getActivity()).name = (session.getUserName());
                        ((LoginActivity2)getActivity()).socialId =  String.valueOf(session.getId());
                        ((LoginActivity2)getActivity()).socialType=  "3";
                         ((LoginActivity2)getActivity()).email = result.data ;

                        ((LoginActivity2)getActivity()).sendLoginDataToServer();

                    }

                    @Override
                    public void failure(TwitterException exception) {
                        // Do something on failure
                        Log.e(TAG," Email "+ exception.toString());
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {

                Log.e(TAG," "+exception.toString());

            }
        });




    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);

        btn_login_twitter.onActivityResult(requestCode, resultCode, data);


    }

    private void getProfileImage()
    {
        Call<User> user = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, false,true);
        user.enqueue(new Callback<User>() {
            @Override
            public void success(Result<User> userResult) {
                String name = userResult.data.name;
                String email = userResult.data.email;

                // _normal (48x48px) | _bigger (73x73px) | _mini (24x24px)
                String photoUrlNormalSize   = userResult.data.profileImageUrl;
                Log.e(TAG," Profile Image "+ photoUrlNormalSize);
                String photoUrlBiggerSize   = userResult.data.profileImageUrl.replace("_normal", "_bigger");
                String photoUrlMiniSize     = userResult.data.profileImageUrl.replace("_normal", "_mini");
                String photoUrlOriginalSize = userResult.data.profileImageUrl.replace("_normal", "");

                new SavedData(getActivity()).setTwitterProfilePicUrl(photoUrlBiggerSize);
            }

            @Override
            public void failure(TwitterException exc) {
                Log.d("TwitterKit", "Verify Credentials Failure", exc);
            }
        });
    }
}
