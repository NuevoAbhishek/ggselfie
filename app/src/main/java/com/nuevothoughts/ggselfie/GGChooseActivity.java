package com.nuevothoughts.ggselfie;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.nuevothoughts.ggselfie.Adapter.AdapterGGChoose;
import com.nuevothoughts.ggselfie.Helper.AlertDialogBox;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.ConnectionDetector;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Interface.ChooseGGImage;
import com.nuevothoughts.ggselfie.Interface.SelectItemFromFilter;
import com.nuevothoughts.ggselfie.Model.GGImage;
import com.nuevothoughts.ggselfie.Utility.CustomFont;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomArrayRequest;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dell1 on 04-Oct-17.
 */

public class GGChooseActivity extends AppCompatActivity implements ChooseGGImage {
    String TAG = "GGChoose";
    int position, imageSelectedPosition;
    static Bitmap bitmap;
    int pixHeight, pixWidth;
    RecyclerView recyclerView;
    List<GGImage> imageList = new ArrayList<GGImage>();
    RelativeLayout rl_bottom;
    AdapterGGChoose adapterGGChoose;
    ImageView imageView;
    ProgressBar progress_bar;
    TextView screenTitle;
    ImageButton btn_left_swipe, btn_right_swipe, btn_back, btn_next;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ggchoose);
        overridePendingTransition(R.anim.enter, R.anim.exit);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        imageView = (ImageView) findViewById(R.id.imageView);
        btn_left_swipe = (ImageButton) findViewById(R.id.btn_left_swipe);
        btn_right_swipe = (ImageButton) findViewById(R.id.btn_right_swipe);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_next = (ImageButton) findViewById(R.id.btn_next);
        rl_bottom = (RelativeLayout) findViewById(R.id.rl_bottom);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        screenTitle = (TextView)findViewById(R.id.screenTitle);
        screenTitle.setTypeface(CustomFont.getAppFont(this));

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        pixHeight = displayMetrics.heightPixels;
        pixWidth = displayMetrics.widthPixels;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(pixWidth, (int) (pixHeight * 0.35));
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        rl_bottom.setLayoutParams(params);


        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        adapterGGChoose = new AdapterGGChoose(this, imageList, this);
        recyclerView.setAdapter(adapterGGChoose);


        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                Intent intent = new Intent(GGChooseActivity.this, CameraActivity.class);
                if (bitmap != null) {
                    startActivity(intent);
                }
                {
                    // Toast.makeText(getApplicationContext()," Select a image")
                }

            }
        });

        btn_left_swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_right_swipe.setVisibility(View.VISIBLE);
                if (imageSelectedPosition > 0) {

                    imageList.get(imageSelectedPosition).setImageSelected(false);
                    recyclerView.smoothScrollToPosition(--imageSelectedPosition);

                    Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + imageList.get(imageSelectedPosition).getImageName()).into(imageView);

                    imageList.get(imageSelectedPosition).setImageSelected(true);
                    adapterGGChoose.notifyDataSetChanged();

                    if (imageSelectedPosition == 0) {

                        btn_left_swipe.setVisibility(View.GONE);
                    }

                }


            }
        });

        btn_right_swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_left_swipe.setVisibility(View.VISIBLE);
                if (imageSelectedPosition < imageList.size() - 1) {

                    imageList.get(imageSelectedPosition).setImageSelected(false);
                    recyclerView.smoothScrollToPosition(++imageSelectedPosition);
                    Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + imageList.get(imageSelectedPosition).getImageName()).into(imageView);

                    imageList.get(imageSelectedPosition).setImageSelected(true);
                    adapterGGChoose.notifyDataSetChanged();
                }

                if (imageSelectedPosition == imageList.size() - 1) {

                    btn_right_swipe.setVisibility(View.GONE);
                }


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        progress_bar.setVisibility(View.GONE);

        if(adapterGGChoose!=null) {
            adapterGGChoose.notifyDataSetChanged();
        }
        if (ConnectionDetector.isConnectingToInternet(getApplicationContext())) {
            downloadGGImages();
        } else {

            showDialog();
        }

    }

    /**
     * if time out error or any time of error found then calling cache data to get the gauti images
     */

    private void cacheCallingToSetGautiImages() {
        Cache cache = AppController.getmInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(Constant.BaseUrl + "app_normal_selfie_frames/getNormalSelfieFrames");
        if (entry != null) {
            try {

                String data = new String(entry.data, "UTF-8");
                // handle data, like converting it to xml, json, bitmap etc.,
                Log.e(TAG, "Data Available " + data);
                ///  AppController.getmInstance().getRequestQueue().getCache().invalidate(Constant.API_URL + "user_matched/already_match_users?access_token=" + Constant.ACCESS_TOKEN, true);

                try {
                    progress_bar.setVisibility(View.GONE);
                    imageList.clear();
                    progress_bar.setVisibility(View.GONE);
                    imageList.clear();
                    GGImage ggImage;
                    JSONObject jsonObject = new JSONObject(data);
                    JSONArray jsonArray = jsonObject.getJSONArray("frames");

                    Log.e(TAG + " array", " " + jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Log.e(TAG + " url", " " + jsonArray.getJSONObject(i).getString("url"));

                        ggImage = new GGImage();
                        ggImage.setImageName(jsonArray.getJSONObject(i).getString("url"));
                        if (i == 0) {
                            ggImage.setImageSelected(true);
                        }

                        imageList.add(ggImage);

                    }


                    adapterGGChoose.notifyDataSetChanged();
                    Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + imageList.get(0).getImageName()).into(imageView);
                    recyclerView.smoothScrollToPosition(0);

                } catch (Exception e) {

                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            // Cached response doesn't exists. Make network call here
            Log.e(TAG, "Data Not Available ");

        }

    }

    @Override
    public void selectedGGClickListener(int position) {


        Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + imageList.get(position).getImageName()).into(imageView);

        imageList.get(imageSelectedPosition).setImageSelected(false);// set previous selected position false
        imageList.get(position).setImageSelected(true);// new selected position

        this.position = position;
        imageSelectedPosition = position;
        adapterGGChoose.notifyDataSetChanged();

        btn_right_swipe.setVisibility(View.VISIBLE);

        btn_left_swipe.setVisibility(View.VISIBLE);
        if (imageSelectedPosition == imageList.size() - 1) {
            btn_right_swipe.setVisibility(View.GONE);
        }
        if (imageSelectedPosition == 0) {
            btn_left_swipe.setVisibility(View.GONE);
        }

    }

    private void downloadGGImages() {

        progress_bar.setVisibility(View.VISIBLE);
        //now create the object of the custom class
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "app_normal_selfie_frames/getNormalSelfieFrames", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress_bar.setVisibility(View.GONE);
                imageList.clear();
                GGImage ggImage;
                Log.e(TAG + " Success", response.toString());

                try {
                    JSONArray jsonArray = response.getJSONArray("frames");

                    Log.e(TAG + " array", " " + jsonArray);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Log.e(TAG + " url", " " + jsonArray.getJSONObject(i).getString("url"));

                        ggImage = new GGImage();
                        ggImage.setImageName(jsonArray.getJSONObject(i).getString("url"));
                        if (i == 0) {
                            ggImage.setImageSelected(true);
                            imageSelectedPosition = 0;
                        }

                        imageList.add(ggImage);

                    }


                    adapterGGChoose.notifyDataSetChanged();
                    Glide.with(getApplicationContext()).load(Constant.ImageDownloadBaseUrl + imageList.get(0).getImageName()).into(imageView);
                    recyclerView.smoothScrollToPosition(0);


                } catch (Exception e) {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());

                cacheCallingToSetGautiImages();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetGGImage");//add the request to the queue
    }


    public void showDialog() {


        AlertDialog.Builder dialog = new AlertDialog.Builder(GGChooseActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Connection");
        dialog.setMessage("Please check your internet connection");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        })
                .setNegativeButton("retry ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".

                        onResume();
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();

    }
}
