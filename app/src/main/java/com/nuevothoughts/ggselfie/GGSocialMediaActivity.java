package com.nuevothoughts.ggselfie;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;


import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nuevothoughts.ggselfie.Adapter.GGSocialMediaAdapter;
import com.nuevothoughts.ggselfie.Fragment.ContestFragment;
import com.nuevothoughts.ggselfie.Fragment.GGFacebookFragment;
import com.nuevothoughts.ggselfie.Fragment.GGInstagramFragment;
import com.nuevothoughts.ggselfie.Fragment.GGTimeLineFragment;
import com.nuevothoughts.ggselfie.Fragment.GGTwitterFragment;
import com.nuevothoughts.ggselfie.Utility.CustomFont;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by dell1 on 20-Sep-17.
 */

public class GGSocialMediaActivity extends AppCompatActivity {
    TabLayout tab;
    String TAG = "GGScial";
    private ViewPager viewPager;
    private GGSocialMediaAdapter mAdapter;
    private ActionBar actionBar;
    public String openedFrom;
    ImageButton btn_back;
    ViewPagerAdapter adapter;
    TextView tv_title;
    // Tab titles
    private String[] tabs = {"fb", "twitter", "instagram", "ggTimeline"};


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ggsocialmedia);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tab = (TabLayout) findViewById(R.id.tab);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        tv_title = (TextView) findViewById(R.id.tv_title);

        tv_title.setTypeface(CustomFont.getAppFont(this));
        openedFrom = getIntent().getStringExtra("openedFrom");
        setUpViewPager(viewPager);

        tab.setupWithViewPager(viewPager);
        tab.getTabAt(0).setIcon(R.drawable.gglogo_small);
        tab.getTabAt(1).setIcon(R.drawable.facebook);
        tab.getTabAt(2).setIcon(R.drawable.twitter);
        tab.getTabAt(3).setIcon(R.drawable.instagram);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (openedFrom.equals("FinishActivity") || openedFrom.equals("LoginActivity2")) {

                    Intent intent = new Intent(GGSocialMediaActivity.this,HomeActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        });

    }

    private void setUpViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContestFragment(), "");
        adapter.addFragment(new GGFacebookFragment(), "");
        adapter.addFragment(new GGTwitterFragment(), "");
        adapter.addFragment(new GGInstagramFragment(), "");
        //  adapter.addFragment(new GGTimeLineFragment(), "");

        viewPager.setAdapter(adapter);

        adapter.notifyDataSetChanged();


        viewPager.setOffscreenPageLimit(5);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragmentList = new ArrayList<>();
        private List<String> fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }

        private void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.e(TAG, "Resume");


    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}
