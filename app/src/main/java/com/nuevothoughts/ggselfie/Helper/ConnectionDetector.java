package com.nuevothoughts.ggselfie.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * class for detecting if user is connected with internet or not
 * Created by Shyamnath Vishwakarma on 17/09/16.
 */
public class ConnectionDetector {

    //function to check and return the connectivity status
    public static boolean isConnectingToInternet(Context context) {

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();

    }

}
