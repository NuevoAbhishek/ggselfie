package com.nuevothoughts.ggselfie.Helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dell1 on 09-Oct-17.
 */

public class SavedData {

    private static SharedPreferences sharedPreferences;
    private String preferenceName = "GGSelfie_Preference_SavedData";
    private SharedPreferences.Editor editor;
    Context context;

    public SavedData(Context pref) {
        if (pref != null) {
            sharedPreferences = pref.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            context = pref;
        }
    }


    private String social_id;
    private String social_type;

    private boolean userFirstTime;

    public String getSocial_id() {
        return sharedPreferences.getString("social_id", null);
    }

    public void setSocial_id(String social_id) {
        editor = sharedPreferences.edit();
        editor.putString("social_id", social_id);
        editor.commit();
    }

    public String getSocial_type() {
        return sharedPreferences.getString("social_type", null);
    }

    public void setSocial_type(String social_type) {
        editor = sharedPreferences.edit();
        editor.putString("social_type", social_type);
        editor.commit();
    }

    public boolean isUserFirstTime() {

        return sharedPreferences.getBoolean("userFirstTime", true);
    }

    public void setUserFirstTime(boolean userFirstTime) {
        editor = sharedPreferences.edit();
        editor.putBoolean("userFirstTime", userFirstTime);
        editor.commit();
    }

    public String getGoogleProfilePicUrl() {
        return sharedPreferences.getString("googgleUrl", null);
    }

    public void setGoogleProfilePicUrl(String googgleUrl) {
        editor = sharedPreferences.edit();
        editor.putString("googgleUrl", googgleUrl);
        editor.commit();
    }

    public String getTwitterProfilePicUrl() {
        return sharedPreferences.getString("twitterUrl", null);
    }

    public void setTwitterProfilePicUrl(String twitterUrl) {
        editor = sharedPreferences.edit();
        editor.putString("twitterUrl", twitterUrl);
        editor.commit();
    }

}
