package com.nuevothoughts.ggselfie;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.clans.fab.FloatingActionButton;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Helper.MyBounceInterpolator;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell1 on 13-Oct-17.
 */

public class HomeActivity extends AppCompatActivity {

    String TAG = "HomeActivity";

    FrameLayout frame_contianer;
    Button btn_login_process;
    boolean IsMenuOpened = false, IsSubMenuOpened = false;
    FloatingActionButton fab_menu;
    LinearLayout ll_submeu_left, ll_submeu_right, ll_sub_icons;
    RelativeLayout rl_top;
    ImageButton gallery, social;
    Animation myAnim;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        TwitterConfig config = new TwitterConfig.Builder(getApplicationContext())
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constant.TWITTER_CONSUMER_KEY, Constant.TWITTER_CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);


        btn_login_process = (Button) findViewById(R.id.btn_login_process);

        fab_menu = (FloatingActionButton) findViewById(R.id.fab_menu);
        ll_submeu_left = (LinearLayout) findViewById(R.id.ll_submeu_left);
        ll_submeu_right = (LinearLayout) findViewById(R.id.ll_submeu_right);
        rl_top = (RelativeLayout) findViewById(R.id.rl_top);
        gallery = (ImageButton) findViewById(R.id.gallery);
        social = (ImageButton) findViewById(R.id.social);
        ll_sub_icons = (LinearLayout) findViewById(R.id.ll_sub_icons);
        frame_contianer = (FrameLayout) findViewById(R.id.frame_contianer);

        myAnim = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Uri uri = Uri.parse(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath()
                        + "/GGSelfie/");


                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_PICK);

                intent.setData(uri);
                intent.setType("image/*");

                startActivityForResult(intent, 0);



            }
        });

        fab_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!IsMenuOpened) {// if false then make is visible

                    IsMenuOpened = true;
                    ll_submeu_right.startAnimation(myAnim);
                    ll_submeu_left.startAnimation(myAnim);
                    // Use bounce interpolator with amplitude 0.2 and frequency 20
                    ll_submeu_right.setVisibility(View.VISIBLE);
                    ll_submeu_left.setVisibility(View.VISIBLE);
                } else {// if true then alreday visible so make it gone


                    if (IsSubMenuOpened) {
                        ll_sub_icons.setVisibility(View.GONE);
                     IsSubMenuOpened = false;
                    }
                    else {
                        IsMenuOpened = false;
                        ll_submeu_right.setVisibility(View.GONE);
                        ll_submeu_left.setVisibility(View.GONE);

                    }
                }



        }
    });


        btn_login_process.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View v){
        Intent intent = new Intent(HomeActivity.this, GGChooseActivity.class);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        startActivity(intent);

    }
    });

        social.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View v){

        Intent intent = new Intent(HomeActivity.this, GGSocialMediaActivity.class);
        intent.putExtra("openedFrom", "HomeActivity");
        startActivity(intent);
    }
    });

        rl_top.setOnTouchListener(new View.OnTouchListener()

    {

        int downX, upX;

        @Override
        public boolean onTouch (View v, MotionEvent event){

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            downX = (int) event.getX();
            Log.e("event.getX()", " downX " + downX);
            return true;
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            upX = (int) event.getX();
            Log.e("event.getX()", " upX " + upX);
            if (upX - downX > 100) {

                // swipe right
            } else if ((downX - upX) >150) {

                // swipe left

                Intent intent = new Intent(HomeActivity.this, GGChooseActivity.class);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                startActivity(intent);
            }
            return true;

        }
        return false;
    }
    });

}


    @Override
    protected void onResume() {
        super.onResume();

        String socialId = new SavedData(getApplicationContext()).getSocial_id();
        String socialType = new SavedData(getApplicationContext()).getSocial_type();

        if (socialId != null && socialType != null) {// checking the login every time when app launch
            checkLogin(socialId, socialType);

        } else {
            Constant.IsUserLoggedIn = false;// if value is null then make it user is not logged in
        }
    }

    /**
     * make login every time user is launching the app.
     *
     * @param socialId
     * @param socialType
     */
    private void checkLogin(String socialId, String socialType) {


        Map<String, String> params = new HashMap<>();
        params.put("social_id", socialId);
        params.put("social_type", socialType);
        Log.e(TAG, " param " + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "GGFUsers/socialLogin", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {

                    Constant.IsUserLoggedIn = true;

                    JSONArray jsonArray = response.getJSONArray("user");

                    for (int i = 0; i < 1; i++) {
                        Constant.ID = jsonArray.getJSONObject(i).getString("id");
                        Constant.EMAIL = jsonArray.getJSONObject(i).getString("email");
                        Constant.NAME = jsonArray.getJSONObject(i).getString("name");
                        Constant.ACCESS_TOKEN = jsonArray.getJSONObject(i).getString("accessToken");

                    }

                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());


                if (error != null && error.networkResponse != null) {
                    Log.e(TAG, "Error " + error.networkResponse.statusCode);
                }
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue


    }

    public void iconClick(View id) {

        Intent intent = null;
        switch (id.getId()) {
            case R.id.profile:
                intent = new Intent(this,ProfileActivity.class);

                break;
            case R.id.sponsors:
                intent = new Intent(this,SponsorsActivity.class);
                break;

            case R.id.contact:

                if (!IsSubMenuOpened) {// if false then make is visible

                    IsSubMenuOpened = true;
                    ll_sub_icons.startAnimation(myAnim);
                    ll_sub_icons.setVisibility(View.VISIBLE);

                } else {// if true then alreday visible so make it gone
                    IsSubMenuOpened = false;
                    ll_sub_icons.setVisibility(View.GONE);

                }

                break;
            case R.id.ggf:

                intent = new Intent(this,GGFActivity.class);
                break;
            case R.id.privacy:

                intent = new Intent(this,PrivacyActivity.class);
                break;
            case R.id.t_c:
                intent = new Intent(this,TAndCActivity.class);
                break;
            case R.id.feedback:

                intent = new Intent(this, FeedbackActivity.class);
                break;
            case R.id.contact_us:

                intent = new Intent(this, ContactUsActivity.class);
                break;

            default:
                break;

        }

        if (intent != null) {

            startActivity(intent);
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        findViewById(R.id.frame_contianer).setVisibility(View.GONE);

    }
}
