package com.nuevothoughts.ggselfie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.nuevothoughts.ggselfie.Utility.ColorFilter2;
import com.nuevothoughts.ggselfie.Utility.CustomSeekBar;


public class ImageCorrectionActivity extends AppCompatActivity {

    ImageView myImage; static Bitmap finalBitmap;
    CustomSeekBar seekbar_saturation, seekBar;
    SeekBar seekbar_contrast, seekbar_temperature, seekbar_exposure, seekBar_hue;
    ImageButton btn_bright, btn_contrast, btn_saturation, btn_temp, btn_gamma, btn_hue,next;
    Button btn_gautiPhoto,btn_myphoto; LinearLayout ll_selected_myphoto,ll_selectedgauti;

    int brightness, saturation, hue, contrast, temperature = 10;float gamma = 1;


    static {
        System.loadLibrary("NativeImageProcessor");
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_correction);


        myImage = (ImageView) findViewById(R.id.myImage);
        seekBar = (CustomSeekBar) findViewById(R.id.seekbar);
        seekbar_contrast = (SeekBar) findViewById(R.id.seekbar_contrast);
        seekbar_saturation = (CustomSeekBar) findViewById(R.id.seekbar_saturation);
        seekbar_temperature = (SeekBar) findViewById(R.id.seekbar_temperature);
        seekbar_exposure = (SeekBar) findViewById(R.id.seekbar_exposure);
        seekBar_hue = (SeekBar) findViewById(R.id.seekBar_hue);

        btn_bright = (ImageButton) findViewById(R.id.btn_bright);
        btn_contrast = (ImageButton) findViewById(R.id.btn_contrast);
        btn_saturation = (ImageButton) findViewById(R.id.btn_saturation);
        btn_temp = (ImageButton) findViewById(R.id.btn_temp);
        btn_gamma = (ImageButton) findViewById(R.id.btn_gamma);
        btn_hue = (ImageButton) findViewById(R.id.btn_hue);
        next = (ImageButton) findViewById(R.id.next);

        btn_gautiPhoto = (Button) findViewById(R.id.btn_gautiPhoto);
        btn_myphoto = (Button) findViewById(R.id.btn_myphoto);
        ll_selected_myphoto = (LinearLayout) findViewById(R.id.ll_selected_myphoto);
        ll_selectedgauti = (LinearLayout) findViewById(R.id.ll_selectedgauti);


       // myImage.setImageResource(R.drawable.image);
         myImage.setImageBitmap(CameraActivity.finalBitmapFromCamera);

        seekBar.setVisibility(View.GONE);
        seekbar_contrast.setVisibility(View.GONE);
        seekbar_saturation.setVisibility(View.GONE);
        seekbar_temperature.setVisibility(View.GONE);
        seekbar_exposure.setVisibility(View.GONE);
        seekBar_hue.setVisibility(View.GONE);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int windowWidth = size.x;
        int windowHeight = size.y;

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                windowHeight*7 /10, windowWidth*7/10);
       // myImage.setLayoutParams(params);
       // myImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        // myImage.buildDrawingCache();

        seekBar.setMax(512);
        seekBar.setProgress(255);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                progress = progress - 255;


            /*
                Log.e("ImageColor_bri global", "" + bitmap_global);

                Bitmap new_bm = ChangeColor.changeBrightness(bitmap_global, (float) progress);
                Log.e("ImageColor_bri local", "" + new_bm);
                myImage.setImageBitmap(new_bm);
                bitmap_local = new_bm;
                */
                brightness = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));

                /// myImage.getDrawable().setColorFilter(ColorFilterGenerator.adjustBrightness(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_saturation.setMax(512);
        seekbar_saturation.setProgress(255);
        //seekbar_saturation.setThumbOffset(255);
        seekbar_saturation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress - 255;
                float prog = progress;
                // myImage.buildDrawingCache();
                // Bitmap bimap = myImage.getDrawingCache();
                //  myImage.setColorFilter(value);

                /*
                Log.e("ImageColor_satu global", "" + bitmap_global);
                Bitmap new_bm = ChangeColor.adjustSaturation(bitmap_global, progress);
                Log.e("ImageColor_satu local", "" + new_bm);
                myImage.setImageBitmap(new_bm);
                bitmap_local = new_bm;
               */
                saturation = progress;
                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));

                // ColorMatrix matrix = new ColorMatrix();
                //  matrix.setSaturation(progress);
                // ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                //myImage.getDrawable().setColorFilter(ColorFilterGenerator.adjustSaturation(progress));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_contrast.setMax(100);

        seekbar_contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                //  myImage.buildDrawingCache();
                // Bitmap bimap = myImage.getDrawingCache();

                //  myImage.setColorFilter(value);
                //float prog = progress * 0.01f;
                contrast = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekbar_temperature.setMax(200);
        seekbar_temperature.setProgress(100);
        seekbar_temperature.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                progress = progress / 10;
                temperature = progress;
                if (progress == 20) {
                    temperature = 19;
                }

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar_exposure.setMax(1000);
        seekbar_exposure.setProgress(500);
        seekbar_exposure.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                ColorMatrix colorMatrix = new ColorMatrix();
                float  prog = progress*0.002f;
                //  myImage.setColorFilter(value);

                gamma = prog;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar_hue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


                hue = progress;

                myImage.getDrawable().setColorFilter(ColorFilter2.adjustColor(brightness, saturation, hue, contrast, temperature,gamma));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalBitmap = ColorFilter2.getFinalBitmap(((BitmapDrawable) myImage.getDrawable()).getBitmap());
                Intent intent = new Intent(ImageCorrectionActivity.this,ApplyFilterOnImageActivity.class);
                startActivity(intent);
              //  finish();
              //  MainActivityCamera.finalBitmapFromCamera.recycle(); MainActivityCamera.finalBitmapFromCamera = null;

            }
        });

        btn_gautiPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ll_selected_myphoto.setBackgroundColor(Color.parseColor("#adadad"));
                ll_selectedgauti.setBackgroundColor(Color.parseColor("#f4b500"));

            }
        });
        btn_myphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_selected_myphoto.setBackgroundColor(Color.parseColor("#f4b500"));
                ll_selectedgauti.setBackgroundColor(Color.parseColor("#adadad"));


            }
        });
    }


    public void setSeekValue(View view) {

        seekBar.setVisibility(View.GONE);
        seekbar_contrast.setVisibility(View.GONE);
        seekbar_saturation.setVisibility(View.GONE);
        seekbar_temperature.setVisibility(View.GONE);
        seekbar_exposure.setVisibility(View.GONE);
        seekBar_hue.setVisibility(View.GONE);

        btn_bright.setImageResource(R.drawable.exposure_gray);
        btn_contrast.setImageResource(R.drawable.contrast_gray);
        btn_saturation.setImageResource(R.drawable.saturation_gray);
        btn_temp.setImageResource(R.drawable.temprature_gray);
        btn_gamma.setImageResource(R.drawable.exposure_gray);
        btn_hue.setImageResource(R.drawable.contrast_gray);


        switch (view.getId()) {
            case R.id.btn_bright: {


                seekBar.setVisibility(View.VISIBLE);
                btn_bright.setImageResource(R.drawable.exposure_yellow);


                break;
            }
            case R.id.btn_contrast: {

                seekbar_contrast.setVisibility(View.VISIBLE);
                btn_contrast.setImageResource(R.drawable.contrast_yellow);
                break;
            }
            case R.id.btn_saturation: {

                seekbar_saturation.setVisibility(View.VISIBLE);
                btn_saturation.setImageResource(R.drawable.saturation_yellow);
                break;
            }
            case R.id.btn_temp: {



                seekbar_temperature.setVisibility(View.VISIBLE);
                btn_temp.setImageResource(R.drawable.temrature_yellow);
                break;
            }
            case R.id.btn_gamma: {

                seekbar_exposure.setVisibility(View.VISIBLE);
                btn_gamma.setImageResource(R.drawable.exposure_yellow);
                break;
            }
            case R.id.btn_hue: {


                seekBar_hue.setVisibility(View.VISIBLE);
                btn_hue.setImageResource(R.drawable.contrast_yellow);
                break;
            }

        }

    }






}
