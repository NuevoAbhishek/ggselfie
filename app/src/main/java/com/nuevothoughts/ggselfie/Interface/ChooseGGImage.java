package com.nuevothoughts.ggselfie.Interface;

import android.graphics.Bitmap;

/**
 * Created by dell1 on 04-Oct-17.
 */

public interface ChooseGGImage {

    //define an method which will be called when user clicks on any of the items
    public void selectedGGClickListener(int position);
}
