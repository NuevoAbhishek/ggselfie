package com.nuevothoughts.ggselfie.Interface;

/**
 * Created by dell1 on 28-Sep-17.
 */

public interface SelectItemFromFilter  {

    //define an method which will be called when user clicks on any of the items
    public void selectedFilterClickListener(int position);

}
