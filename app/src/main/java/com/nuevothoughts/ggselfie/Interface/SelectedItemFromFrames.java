package com.nuevothoughts.ggselfie.Interface;

/**
 * Created by dell1 on 05-Oct-17.
 */

public interface SelectedItemFromFrames {
    //define an method which will be called when user clicks on any of the items
    public void selectedFramesClickListener(int position);

}
