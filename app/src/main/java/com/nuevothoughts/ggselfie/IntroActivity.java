package com.nuevothoughts.ggselfie;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Fragment.Intro1Fragment;
import com.nuevothoughts.ggselfie.Fragment.Intro2Fragment;
import com.nuevothoughts.ggselfie.Fragment.Intro3Fragment;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rd.pageindicatorview.view.PageIndicatorView;
import com.rd.pageindicatorview.view.animation.AnimationType;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

/**
 * Created by dell1 on 20-Sep-17.
 */

public class IntroActivity extends AppCompatActivity {

    String TAG = "SplashActivity";

    private List<Fragment> fragmentList = new ArrayList<>();
    android.support.design.widget.TabLayout dots;
    ViewPager viewPager;
    int PAGE_COUNT = 4;
    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        viewPager = (ViewPager) findViewById(R.id.viewPager);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        setUpAdapter();

        viewPager.setAdapter(adapter);

        final PageIndicatorView pageIndicatorView = (PageIndicatorView) findViewById(R.id.pageIndicatorView);
        pageIndicatorView.addViewPager(viewPager);
        pageIndicatorView.setCount(3);
        pageIndicatorView.setRadius(5);

        pageIndicatorView.setVisibility(View.VISIBLE);
//set color
        pageIndicatorView.setUnselectedColor(Color.parseColor("#ff0000"));
        pageIndicatorView.setSelectedColor(Color.parseColor("#ffffff"));

//set animation
        pageIndicatorView.setAnimationDuration(500);
        pageIndicatorView.setAnimationType(AnimationType.SLIDE);
        pageIndicatorView.setInteractiveAnimation(true);

//set selection
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int selectedPageIndex = -1;
            private boolean exitWhenScrollNextPage = false;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                if (exitWhenScrollNextPage && position == PAGE_COUNT - 1) {
                    exitWhenScrollNextPage = false; // avoid call more times
                    Log.e(TAG, "onPageScrolled " + position);

                    Intent intent = new Intent(IntroActivity.this, GGChooseActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onPageSelected(int position) {

                Log.e(TAG, "page selected " + position);
                selectedPageIndex = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                if (state == SCROLL_STATE_IDLE) {
                    exitWhenScrollNextPage = selectedPageIndex == PAGE_COUNT - 1;
                }
            }
        });


    }

    private void setUpAdapter() {
        adapter.addFragment(new Intro1Fragment());
        adapter.addFragment(new Intro2Fragment());
        adapter.addFragment(new Intro3Fragment());

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        private void addFragment(Fragment fragment) {
            fragmentList.add(fragment);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();



        if (!(new SavedData(getApplicationContext()).isUserFirstTime())) {

            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
            finish();


        }
    }


}
