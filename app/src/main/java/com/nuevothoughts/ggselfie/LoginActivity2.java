package com.nuevothoughts.ggselfie;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Fragment.ContestFragment;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.Service.EnterInContestService;
import com.nuevothoughts.ggselfie.Utility.AndroidMultiPartEntity;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell1 on 06-Oct-17.
 */

public class LoginActivity2 extends AppCompatActivity {


    public String socialId;
    public String name;
    public String socialType;
    public String email;
    public String phone;
    ProgressDialog progressDialog;
    String openedFrom;
    String storedImagePath;
    String contestTitle, contestDescription, endTime;
    String TAG = "LoginActivity2";
    ProgressBar progressBar;
    Boolean imageUploadSuccess = false;
    BroadcastReceiver enterInContest = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Boolean success = intent.getBooleanExtra("Success", false);
            progressBar.setVisibility(View.GONE);
            if (success) {

                Intent intent1 = new Intent(context,GGSocialMediaActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent1.putExtra("openedFrom","LoginActivity2");
                startActivity(intent1);
                finish();


            } else {

                Toast.makeText(getApplicationContext(),"Oops! try again later",Toast.LENGTH_SHORT).show();
            }

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login2);


        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constant.TWITTER_CONSUMER_KEY, Constant.TWITTER_CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);


        openedFrom = getIntent().getStringExtra("openedFrom");
        storedImagePath = getIntent().getStringExtra("storedImagePath");

    }

    public void sendLoginDataToServer() {


        registerUser();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FragmentManager fragment = getSupportFragmentManager();
        if (fragment != null) {
            fragment.findFragmentById(R.id.twitter_fragment).onActivityResult(requestCode, resultCode, data);
        }
    }

    private void registerUser() {

        progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage("Registration process...");
        Map<String, String> params = new HashMap<>();
        params.put("social_id", socialId);
        params.put("social_type", socialType);// (1- Facebook ,2 - Google , 3- Twitter )
        params.put("email", email);
        params.put("name", name);
        Log.e(TAG, " registration " + params);
        //  params.put("mobile_number", phone);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "GGFUsers/socialRegistration", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                progressDialog.dismiss();
                try {
                   // Toast.makeText(getApplicationContext(), "Login with " + socialType + " UserName is " + name + " id " + socialId + " EmailId " + email, Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Registration successful ", Toast.LENGTH_LONG).show();
                    Constant.IsUserLoggedIn = true;
                    JSONArray jsonArray = response.getJSONArray("user");

                    for (int i = 0; i < 1; i++) {
                        Constant.ID = jsonArray.getJSONObject(i).getString("id");
                        Constant.EMAIL = jsonArray.getJSONObject(i).getString("email");
                        Constant.NAME = jsonArray.getJSONObject(i).getString("name");
                        Constant.ACCESS_TOKEN = jsonArray.getJSONObject(i).getString("accessToken");

                    }

                    new SavedData(getApplicationContext()).setSocial_id(socialId);
                    new SavedData(getApplicationContext()).setSocial_type(socialType);

                   if (openedFrom.equals("FinishActivity"))
                    {
                        getActiveContest();
                    }else if(openedFrom.equals("ContestFragment")) {

                        finish();
                   }else {
                       finish();
                   }

                } catch (Exception e) {

                Log.e(TAG,"Exception "+ e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());

                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Oops! something wrong happened, try again later",Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "RegisterUser");//add the request to the queue


    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(enterInContest,
                new IntentFilter(Constant.ContestBroadcastName)
        );

    }

    @Override
    protected void onPause() {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(enterInContest);

    }

    /**
     * get the live contest
     */
    private void getActiveContest() {
        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("fetching live contest...");
        progressDialog.show();
        //now create the object of the custom class
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.GET, Constant.BaseUrl + "contests/getAllActiveContests", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success " + response);
                try {



                    JSONArray jsonArray = response.getJSONArray("contest");

                    for (int i = 0; i < jsonArray.length(); i++) {


                        contestTitle = jsonArray.getJSONObject(i).getString("title");
                        contestDescription = jsonArray.getJSONObject(i).getString("description");
                        jsonArray.getJSONObject(i).getString("contestType");
                        endTime = jsonArray.getJSONObject(i).getString("end_date");

                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        String time = simpleDateFormat.format(c.getTime());
                        Log.e("SendChat ", time);

                        Date dateDB = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(time.replaceAll("Z$", "+0000"));
                        long curTime = (dateDB.getTime());// this is timestamp of date saved in db

                        Date dateChat = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(endTime.replaceAll("Z$", "+0000"));
                        long endT = (dateChat.getTime());// this is the timestamp of chat coming from server

                        Log.e(TAG, " Success " + contestTitle + " " + contestDescription + " " + curTime + " " + endT);

                        Constant.CONTEST_ID =  jsonArray.getJSONObject(i).getString("contest_id");

                        break;
                    }

                    progressDialog.dismiss();
                    new LoginActivity2.UploadImage().execute();

                    progressDialog = new ProgressDialog(getApplicationContext());
                    progressDialog.setMessage("uploading image for contest...");
                    progressDialog.show();
                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "No live contest found, try again later ", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue


    }

    public class UploadImage extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {


            uploadImage(storedImagePath.toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if (imageUploadSuccess) {
               progressBar.setVisibility(View.VISIBLE);
            }
        }
    }


    //function to upload the image
    private void uploadImage(String filePath) {
        //upload the data to the server
        String responseString = null;
        HttpClient httpclient = new DefaultHttpClient();


        HttpPost httpPost = new HttpPost(Constant.ImageUploadUrl);


        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(new AndroidMultiPartEntity.ProgressListener() {
                @Override
                public void transferred(long num) {
                    Log.e("Image Uploaded: ", "" + num);
                    //publishProgress((int) ((num / (float) totalSize) * 100));//publish the progress report
                }
            });

            File sourceFile = new File(filePath);

            entity.addPart("image", new FileBody(sourceFile));

            httpPost.setEntity(entity);

            //make server call
            HttpResponse response = httpclient.execute(httpPost);
            HttpEntity r_entity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {//if success
                //get the server response
                responseString = EntityUtils.toString(r_entity);
                imageUploadSuccess = true;
                /** // Delete file
                 File fdelete = new File(filePath);
                 if (fdelete.exists()) {
                 if (fdelete.delete()) {
                 Log.e("Image Upload : " , "deleted "+ filePath );
                 } else {

                 }
                 }
                 **/
                progressDialog.dismiss();
                Intent intent = new Intent(this, EnterInContestService.class);
                String imageName=filePath.substring(filePath.lastIndexOf("/")+1);
                intent.putExtra("imageName",imageName );
                startService(intent);

            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "something bad occurred, try again later", Toast.LENGTH_SHORT).show();
            }
        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

        Log.e("Image Upload Error", responseString);
        //Toast.makeText(getApplicationContext(), "Response" + responseString, Toast.LENGTH_LONG).show;
    }

}
