package com.nuevothoughts.ggselfie.Model;

/**
 * Created by dell1 on 04-Oct-17.
 */

public class GGImage {

    private String imageName;
    private boolean imageSelected;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public boolean isImageSelected() {
        return imageSelected;
    }

    public void setImageSelected(boolean imageSelected) {
        this.imageSelected = imageSelected;
    }
}
