package com.nuevothoughts.ggselfie.Model;

/**
 * Created by dell1 on 12-Oct-17.
 */

public class ModelForContest {

    private String imageName;
    private String totalLikes;
    private String IsUserLiked;
    private String selfieId;
    private String userId;
    private String selfieDate;
    private String name;
   private boolean callWithUserId;



    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(String totalLikes) {
        this.totalLikes = totalLikes;
    }

    public String isUserLiked() {
        return IsUserLiked;
    }

    public void setUserLiked(String userLiked) {
        IsUserLiked = userLiked;
    }

    public String getSelfieId() {
        return selfieId;
    }

    public void setSelfieId(String selfieId) {
        this.selfieId = selfieId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSelfieDate() {
        return selfieDate;
    }

    public void setSelfieDate(String selfieDate) {
        this.selfieDate = selfieDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCallWithUserId() {
        return callWithUserId;
    }

    public void setCallWithUserId(boolean callWithUserId) {
        this.callWithUserId = callWithUserId;
    }
}
