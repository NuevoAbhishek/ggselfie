package com.nuevothoughts.ggselfie.Model;

import android.graphics.Bitmap;

/**
 * Created by dell1 on 10-Oct-17.
 */

public class ModelForFilter {

    private Bitmap filterBitmap;
    private String filterName;
    private Boolean IsFilterSelected;

    public Bitmap getFilterBitmap() {
        return filterBitmap;
    }

    public void setFilterBitmap(Bitmap filterBitmap) {
        this.filterBitmap = filterBitmap;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public Boolean getFilterSelected() {
        return IsFilterSelected;
    }

    public void setFilterSelected(Boolean filterSelected) {
        IsFilterSelected = filterSelected;
    }
}
