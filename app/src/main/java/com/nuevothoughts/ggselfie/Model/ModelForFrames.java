package com.nuevothoughts.ggselfie.Model;

import android.graphics.Bitmap;

/**
 * Created by dell1 on 05-Oct-17.
 */

public class ModelForFrames {

    private Bitmap filterBitmap;
    private String frameName;

    private Boolean IsSelected;
    private String text;
    public Bitmap getFilterBitmap() {
        return filterBitmap;
    }

    public void setFilterBitmap(Bitmap filterBitmap) {
        this.filterBitmap = filterBitmap;
    }

    public String getFrameName() {
        return frameName;
    }

    public void setFrameName(String frameName) {
        this.frameName = frameName;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getSelected() {
        return IsSelected;
    }

    public void setSelected(Boolean selected) {
        IsSelected = selected;
    }
}
