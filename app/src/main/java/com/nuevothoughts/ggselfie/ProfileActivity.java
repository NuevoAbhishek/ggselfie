package com.nuevothoughts.ggselfie;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Helper.SavedData;
import com.nuevothoughts.ggselfie.Utility.CustomFont;

/**
 * Created by dell1 on 17-Oct-17.
 */

public class ProfileActivity extends AppCompatActivity {

    TextView screenTitle, name, email;
    RoundedImageView userImage;
    String TAG = "Profile";
    ImageButton btn_back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile);

        screenTitle = (TextView) findViewById(R.id.screenTitle);
        screenTitle.setTypeface(CustomFont.getAppFont(this));
        name = (TextView) findViewById(R.id.name);
        name.setTypeface(CustomFont.getAppFont(this));

        email = (TextView) findViewById(R.id.email);
        email.setTypeface(CustomFont.getAppFont(this));

        userImage = (RoundedImageView) findViewById(R.id.userImage);
        btn_back = (ImageButton) findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();


        if (Constant.IsUserLoggedIn) {

            name.setText(Constant.NAME);
            email.setText(Constant.EMAIL);
            if (Integer.parseInt(new SavedData(this).getSocial_type()) == 1) {// 1== facebook,2==google,3== twitter

                Log.e(TAG, "id " + Constant.SOCIAL_ID);
                Glide.with(this).load("https://graph.facebook.com/" + new SavedData(this).getSocial_id() + "/picture?width=9999&height=9999").into(userImage);


            } else if (Integer.parseInt(new SavedData(this).getSocial_type()) == 2) {

                if(!new SavedData(this).getGoogleProfilePicUrl().equals("null")) {
                    Glide.with(this).load(new SavedData(this).getGoogleProfilePicUrl()).into(userImage);
                }

            } else if (Integer.parseInt(new SavedData(this).getSocial_type()) == 3) {
                if(!new SavedData(this).getTwitterProfilePicUrl().equals("null")) {
                    Glide.with(this).load(new SavedData(this).getTwitterProfilePicUrl()).into(userImage);
                }

            }


        } else {

            showDialog();
        }

    }

    public void showDialog() {


        AlertDialog.Builder dialog = new AlertDialog.Builder(ProfileActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Login");
        dialog.setMessage("No login information, Please login");
        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".

                Intent intent = new Intent(getApplication(), LoginActivity2.class);
                intent.putExtra("openedFrom","ProfileActivity");
                startActivity(intent);
            }
        })
                .setNegativeButton("Not now ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    finish();

                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();

    }
}
