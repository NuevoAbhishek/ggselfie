package com.nuevothoughts.ggselfie.Service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.nuevothoughts.ggselfie.Helper.AppController;
import com.nuevothoughts.ggselfie.Helper.Constant;
import com.nuevothoughts.ggselfie.Utility.VolleyCustomRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dell1 on 12-Oct-17.
 */

public class EnterInContestService extends IntentService {

    String TAG = "InContestService";
    public EnterInContestService() {

        super(" ");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {



        callingMediaApiTogetMediaId( intent.getStringExtra("imageName"));
    }

    private void callingMediaApiTogetMediaId(String imageName) {


        Map<String, String> params = new HashMap<>();
        params.put("url", imageName);
        params.put("type", "1");
        Log.e(TAG,"param Media" + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "media", params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "SuccessMedia " + response);
                try {

                    response.getString("id");
                    callingSelfieAdd( response.getString("id"));

                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());


                Intent intent = new Intent(Constant.ContestBroadcastName);
                intent.putExtra("Success",false);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue

    }

    private void callingSelfieAdd(String media_id) {


        Map<String, String> params = new HashMap<>();
        params.put("media_id", media_id);
        params.put("user_id", Constant.ID);
        params.put("contest_id", Constant.CONTEST_ID);
        Log.e(TAG,"param Selfie" + params);
        VolleyCustomRequest josnRequest = new VolleyCustomRequest(Request.Method.POST, Constant.BaseUrl + "selfies/addSelfie?access_token="+Constant.ACCESS_TOKEN, params, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "Success SelfieAdd " + response);
                try {

                    Intent intent = new Intent(Constant.ContestBroadcastName);
                    intent.putExtra("Success",true);

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);


                } catch (Exception e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " Error", error.toString());


                Intent intent = new Intent(Constant.ContestBroadcastName);
                intent.putExtra("Success",false);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                //  headers.put("clientKey", "" + Constant.CLIENT_KEY);
                return headers;
            }
        };

        //Set a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions.
        josnRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constant.TIMEOUT_MS,//wait 7 seconds
                Constant.MAX_RETRIES,//DefaultRetryPolicy.DEFAULT_MAX_RETRIES, this is 1 //retry for two times
                Constant.BACKOFF_MULT));//DefaultRetryPolicy.DEFAULT_BACKOFF_MULT 1

        AppController.getmInstance().addToRequestQueue(josnRequest, "GetActiveContest");//add the request to the queue

    }
}
