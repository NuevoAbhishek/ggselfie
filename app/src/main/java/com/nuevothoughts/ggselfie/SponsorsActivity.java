package com.nuevothoughts.ggselfie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nuevothoughts.ggselfie.Utility.CustomFont;

/**
 * Created by dell1 on 16-Oct-17.
 */

public class SponsorsActivity extends AppCompatActivity {

    TextView screenTitle;
    ImageButton btn_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sponsors);
        screenTitle = (TextView)findViewById(R.id.screenTitle);

        screenTitle.setTypeface(CustomFont.getAppFont(this));
        btn_back = (ImageButton)findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }
}
