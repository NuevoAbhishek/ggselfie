package com.nuevothoughts.ggselfie.Utility;

import android.content.Context;
import android.graphics.Typeface;


public class CustomFont {

    /**
     * return the app typeface
     *
     * @param context
     * @return
     */
    public static Typeface getAppFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/Phenomena_Bold.otf");
        return typeface;
    }


    /**
     * return the frame typeface
     *
     * @param context
     * @return
     */
    public static Typeface getFrameFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font/KOMIKAX.ttf");
        return typeface;
    }


}
